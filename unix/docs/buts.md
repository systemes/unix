# Buts pédagogiques 🞋

Cet enseignement est construit pour vous faire apprendre **les
fondamentaux des systèmes d'exploitations en les pratiquant**, et
Unix/Linux en particulier. À votre rythme. Néanmoins, il n'y a que
7h30 de TP encadrés en septembre. Il sera important de continuer à pratiquer
pendant le reste de votre cursus. La deuxième partie de janvier traite
des aspects plus avancés qui demande d'avoir déjà de l'expérience.

 ⛏ Les bases creusées dans l'ensemble de ce cours sont les mêmes pour tout le
monde. C'est leurs profondeurs qui varient suivant votre niveau de départ.

  * **comprendre les fondamentaux d'un système d'exploitation** de la
    famille Unix, en particulier, ses principaux objets: les processus
    (les programmes que vous exécutez), les fichiers (qui stockent vos
    données et vos programmes), les répertoires, les liens
    symboliques, les droits, le parallélisme d'exécution,
    l'environnement d'un processus, les opérations possibles sur les
    processus et les fichiers et la façon de les exprimer.
  * maîtriser les **commandes de base** d'UNIX pour savoir manipuler
    programmatiquement ses principaux objets (ls, cd, mkdir, mv, rm, ln, ps,
    kill, cat, echo, grep, |, >, <, &, C-c, C-z, bg, fg)
  * savoir utiliser efficacement **votre terminal**, l'outil informatique
    central que vous utiliserez tout le temps. Ce point parait
    anecdotique, mais comme vous l'utiliserez tout le temps, chaque
    gain sera très rentable (joker, historique, redirection, pipe, ^Z
    vs ^C, raccourcis (recherche, déplacement, modifications))
  * **automatiser** votre travail avec `Bash` 
  * les manipulations de **textes**, à la fois dans les
    contenus des fichiers et dans les noms des fichiers (regexp, sed)
  * gérer **l'histoire de vos fichiers** avec `Git`
  * travailler à distance avec `Ssh` et `scp`
  * Les **éditeurs de texte** universels: les immortels Vim et Emacs,
    ces éditeurs dont certains concepts, et leurs raccourcis clavier,
    sont omniprésents, et VS Code l'éditeur universel à la mode du
    moment (durée de vie moyenne de tous ces prédécesseurs 4-5 ans).
  * Divers: configurer votre lecteur d'émail; imprimer; installer
    Ubuntu sur votre laptop perso; X11; vpn, tmux, etc.
  * Que tous vos comptes fonctionnent (⛐ vous avez en fait plusieurs
    comptes avec la même authentification. Chacun de ces comptes peut
    avoir un souci de connexion isolé): login, email, vpn, intranet,
    gitlab, chamilo
  * Comprendre ce que font les machines virtuelles et les conteneurs
  * Quelques points annexes rigolos: mutt, git (modèle de données,
    flot, etc.), tmate, etc.
