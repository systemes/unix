## Matériel informatique et gestion des comptes

### Gestion des comptes à l'Ensimag

Chaque étudiant a à sa disposition des comptes sur plusieurs machines. Le contenu des comptes (i.e. les fichiers et répertoires qu'il contient) est partagé entre la plupart des machines (PC, machines virtuelles, ...). Voici ce que vous devez savoir pour bien travailler :

* Le répertoire `$HOME/` (qui s'appelle aussi `~`) est partagé entre toutes les machines de l'école (PC et serveurs), donc, par exemple, une modification du fichier `$HOME/.bashrc` sur un PC sera prise en compte sur tous les autres PCs, `pcserveur.ensimag.fr`, ...

* Votre compte est limité par un quota de quelques Gigaoctets (variable selon les années). C'est largement suffisant pour faire vos TP et projets dans de bonnes conditions, mais faites attention à ne pas stocker de contenu trop volumineux (l'intégrale du cours d'archi, par exemple).

* Votre compte est limité par un quota de deux cent mille fichiers. Si vous utilisez VS Code, ou programmer avec Node.JS vous atteindrez cette limite. 

Pour savoir quels répertoires contiennent vos fichiers, exécutez la commande :

``` console
you@ensipc$ du -hs --inodes ~/* ~/.* | sort -h
```

### Répertoires partagés entre étudiants

Pour travailler à plusieurs sur un TP, il peut être pratique d'avoir un répertoire partagé entre plusieurs étudiants, i.e. un répertoire où plusieurs utilisateurs Unix ont les droits en lecture et en écriture. À l'Ensimag, il y a deux choses à savoir :

* On ne peut pas donner la permission à un autre utilisateur sur le contenu de son `$HOME` ;
* Pour les projet en équipe, pour partager vos codes la méthode recommandé est l'utilisation du [serveur gitlab de l'ensimag](https://gitlab.ensimag.fr). Les Bug Busters ont écrit une documentation sur l'utilisation de cette plateforme ici : [documentation gitlab](https://bugbusters.pages.ensimag.fr/wiki/ensimag/gitlab/).

### Imprimer depuis l'Ensimag

Se référer à la documentation écrite par les Bug Busters : [imprimer à l'école](https://bugbusters.pages.ensimag.fr/wiki/ensimag/impression/).

### Accès aux mails

Se référer à la documentation écrite par les Bug Busters : [mail](https://bugbusters.pages.ensimag.fr/wiki/ensimag/email/).

### Gestion de son mot de passe

On peut changer son mot de passe Ensimag en cliquant sur le lien suivant : [gestion des mots de passe](https://copass-client.grenet.fr/).

## Outils pédagogiques

Cette section recense les principales applis web dont vous vous servirez pendant votre scolarité à l'Ensimag.
Une liste plus complète peut être trouvée ici : [portail des BuBus](https://bugbusters.pages.ensimag.fr/wiki/portail/).

### TEIDE

[TEIDE](http://teide.ensimag.fr/) est une application web utilisée à l'école pour le rendu de projets et de TP (archives), et la constitution d'équipes ;

### Emploi du temps en ligne

Vous pouvez consulter votre emploi du temps sur [edt.grenoble-inp.fr](https://edt.grenoble-inp.fr).

### Consulter ses notes

Les notes obtenues aux examens sont accessibles sur [Zenith (avec le VPN)](https://extranet.ensimag.fr/zenith).

### Intranet de l'école

L'intranet de l'Ensimag est accessible ici : [intranet](https://intranet.ensimag.grenoble-inp.fr).

