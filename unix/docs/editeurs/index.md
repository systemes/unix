# Les éditeurs de texte et les langages de programmation

Un éditeur de texte permet avant tout d'éditer du texte brut : une
suite de caractères. En Informatique, on trouve du texte brut
partout : un programme écrit dans un langage de programmation
classique est un fichier texte, une page HTML est un fichier texte, la
plupart des fichiers de configuration des logiciels en sont
également...

Il est important pour vous d'avoir un éditeur avec un bon support pour
le langage que vous êtes en train de manipuler. C'est un énorme gain
de temps, car il va pointer immédiatement vos lacunes en utilisant les
outils disponibles pour vous y aider. Lorsque vous serez plus
expérimenté, cela sera encore important pour éviter quelques fautes
d'inattention, mais vous apprécierez surtout d'avoir sous la main la
documentation de votre propre projet, sa capacité de modification
(refactorisation du code), sa capacité d'insertion de patron de code,
de débogage intégré, etc.

Il y a de très nombreux langages utilisés. Et de nouveaux sont créés
régulièrement. En 2007, la conférence HOPL III, sur l'histoire des
langages de programmations, a dénombré plus de 8 500 langages créés
depuis le début de l'informatique. Certains ont disparu, ou sont
devenus anecdotiques, mais ce décompte de 8 500 n'inclut pas 20% du
top50 de la popularité des langages qui sont apparus après 2007
((TIOBE Index)[https://www.tiobe.com/tiobe-index/]) : Go, Rust, Node.js,
Julia, Elixir, Raku, Swift, Kotlin, TypeScript, Dart, Clojure et Nim.

Pour écrire ces langages, vous avez **deux types d'éditeurs** de
texte : **les spécialisés** et les **généralistes**. Les éditeurs
spécialisés dans quelques langages vont très bien faire ces quelques
langages. Par exemple, [Spyder 5](spyder) est un excellent éditeur
pour Python. Néanmoins, il sera un choix moyen, voir mauvais, pour
presque tous les autres langages.

À L'Ensimag, vous utiliserez une quinzaine de langages différents dès
la première année, et ce nombre va croitre encore en 2A, 3A, puis
dans votre vie professionnelle. Nous vous conseillons donc d'apprendre à
vous servir de très bons éditeurs généralistes.

Nous en avons sélectionné trois pour leurs complémentarités et leurs
utilités. 

Et pour savoir celui qui vous convient à vous, et dans quel cadre,
nous vous conseillons d'apprendre sérieusement les bases de ces trois
éditeurs.

- [Vim, ou neovim](vim), pour sa capacité d'**édition ultra-rapide**
  et son **extensibilité** pour en faire un IDE moderne. Il a hérité
  des commandes de son grand-père, _ed_ (1969), qui était l'éditeur
  standard UNIX avant l'invention des écrans. Vous ne voulez pas
  perdre du temps quand il faut une minute d'impression par ligne de
  code. Son grand-oncle, _sed_, le frère de _ed_, est toujours un
  outil classique de manipulation automatique de flot de texte
  (_Stream EDitor_) que vous utiliserez régulièrement.
- [Emacs](emacs), un **langage** de programmation dynamique **camouflé
  en éditeur**. Sa compétence unique est de pouvoir **combiner ses
  dizaines de milliers de fonctions**. Les autres éditeurs ont des
  greffons (plugins) qui permettent d'additionner des fonctionnalités,
  les unes à côté des autres. Emacs les combinent. Cela lui donne des
  capacités hors normes. Vous l'apprécierez avec votre progression,
  car vous pourrez injecter votre expérience dans cet éditeur et le
  façonner à vos besoins. _Org-mode_ est sa killer-application du
  moment qui rend jaloux les autres IDE.
- [Visual Studio Code](vscode), l'éditeur de texte (de Microsoft) à la
  mode. Lui aussi demande une forte courbe d'apprentissage dans sa
  configuration, mais il le fait en masquant les difficultés pour que
  cela apparaisse facile (clic sur un bouton, 90% de
  réussite). Contrairement aux deux autres, vous devez être conscient
  que c'est un problème pour votre compréhension de ce qui se passe,
  et qu'il est à la mode pour quelques années seulement. Dans les 20
  dernières années, ce rôle de troisième éditeur à la mode à l'Ensimag
  a été tenu successivement par : nedit, netbeans, gedit, eclipse,
  sublimetext, atom, et maintenant VS Code. 7 éditeurs sur 20 ans.
  
  Il y a d'autres challengers libres comme kate (qui ressemble
  visuellement à VS code, mais ne masque rien), helix, qtcreator,
  kdevelop, code::blocks, geany, nano, ou des challengers propriétaires,
  comme la famille JetBrains (Fleet, IntelliJ, CLion, Goland, Rider,
  etc.), SublimeText, ou XCode (macOS).
  
