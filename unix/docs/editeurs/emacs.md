Cette page est écrite pour permettre à des débutants avec l'éditeur de
texte **Emacs** de bien démarrer, en utilisant le langage Python comme
exemple.

Une page similaire est disponible pour Visual Studio Code :
[Premiers pas avec VS Code et Python](vscode.md) et
[VIM](vim.md). Certaines options expliquées ici ont peut-être déjà été
activées par défaut dans la configuration d'Emacs par défaut de
l'Ensimag.


Contenu de cette page :
- [Emacs, premier contact à l'Ensimag](#emacs-premier-contact)
- [Emacs, exemple d'utilisation comme éditeur Python](#emacs-editeur-python)
- [Emacs, quelques configurations maximalistes qui brillent dans le noir](#emacs-config-qui-brillent)

Emacs… premier démarrage avec la config Ensimag 🪛 {#emacs-premier-contact}
---------------------------------------------------

Emacs est un éditeur de texte, comme VS Code ou VIM, programmable et
extensible. Vous pouvez avoir une idée des nombreuses fonctionnalités de base en
lisant la page web [emacs tour](https://www.gnu.org/software/emacs/tour/)

Mais cette façon de voir est trompeuse. Emacs est un langage de
programmation interactif (LISP, le langage à l'origine la première
révolution de l'IA dans les années 1970), donc tout est programmable,
remplaçable ou combinable à la volée.

Sans parler de lire les cartes OpenStreetMap, annoter vos documents
PDF ou jouer à sokoban, vous pouvez comparer les visuels de deux
**themes** au deux extrêmes :

-  [nano-emacs, style zen ultra-minimaliste](https://github.com/rougier/nano-emacs)
-  [spacemacs, style VS Code](https://www.spacemacs.org/)

Si vous changez la configuration pour mettre des [configurations
avancées](#emacs-config-qui-brillent), lors du premier démarrage,
emacs va télécharger la dernière version sur Internet des
modules. Cela prend du temps !

**Lancez maintenant pour la première fois emacs**

`emacs`

**et patientez** le temps du chargement si besoin.

Emacs… premiers pas 👣
-------------------------

Le tutoriel, en français, est accessible dans le menu HELP

![](img/emacs/Emacs-tutorial.png "Emacs-tutorial.png")


Emacs… ne pas utiliser la configuration Ensimag 🪛
--------------------------------------------------

Il est toujours possible d'utiliser emacs sans aucune configuration
utilisateur en utilisant lançant votre éditeur avec l'option *-q*

`emacs -q`


Emacs… config Ensimag: elle demande votre projet ! 🪛
--------------------------------------------------------

Vos codes seront bientôt tous réalisés au sein de projets Git.
La configuration par défaut à l'Ensimag cherche donc ce projet.

Si Emacs ne trouve pas de projet, **il va vous demander d'indiquer le
projet, ou de préciser qu'il n'y a pas de projet** (Message sur la
ligne du bas de l'éditeur, le mini-buffer)

Dans les premiers temps, vous devrez donc répondre **n** (No) car
votre code n'est pas au sein d'un projet Git.


Emacs… un éditeur de texte 🪶 {#emacs-editeur-python}
--------------------------------

Emacs est un éditeur de texte, très puissant, extensible, même s'il
n'est pas toujours très *intuitif* (au sens qu'il fait des choix
différents de vos habitudes).  [Il peut parler (interface pour
non-voyants avec de multiples adaptations auditive au
contexte)](https://emacspeak.blogspot.com/). [Il peut controler vos
autres fenêtres](https://github.com/emacs-exwm/exwm). Certains disent
qu'il peut tout faire sauf le café. [Ils se trompent, il fait aussi le
café](https://www.emacswiki.org/emacs/CoffeeMode).

D'abord, qu'est-ce que ça veut dire, un « Éditeur de texte » ? Vous
avez sûrement déjà utilisé un « *Traitement* de texte », qui permet de
taper du texte, de le mettre en forme (gras, italique, taille et choix
des polices,…). Un éditeur de texte édite du texte brut : pas de mise
en forme, pas de dessins, juste du texte. Une suite de caractères, quoi.
Mais en informatique, on trouve du texte brut partout : langage de
programmation, fichiers de configuration de beaucoup de logiciels…

On pourrait essayer d'éditer le texte d'un programme Python avec un
traitement de texte classique, mais comme les fichiers créés par ces
logiciels ne sont pas des fichiers textes bruts, nous aurions de sérieux
problèmes au moment de l'exécution du programme Python.

### Ouvrir un fichier

Vous devriez avoir un fichier `hello.py` dans le répertoire `stage-unix`
sur votre compte. Si ce n'est pas le cas, commencez par télécharger le
fichier
[hello.py](https://systemes.pages.ensimag.fr/unix-gitlab/editeurs/hello.py),
et sauvegardez-le sur votre compte.

Un moyen simple d'ouvrir ce fichier dans Emacs est de lancer, en ligne
de commande :

`emacs hello.py`

**Pensez à appuyer sur "n"** pour indiquer que le ficher n'appartient
pas à un projet Git (dans la configuration par défaut à l'Ensimag)

Si vous voulez lancer emacs sans la configuration de l'Ensimag, il
suffit de lancer plutôt

`emacs -q hello.py`

Un autre moyen, c'est de lancer Emacs, puis dans le menu `File`,
choisir `Open File…`, et choisir le fichier à ouvrir.

![](img/emacs/Emacs-open-hello-py.png "Emacs-open-hello-py.png")

Quand on a plusieurs fichiers sur lesquels on veut travailler en même
temps, il *faut* ouvrir chaque fichier depuis une fenêtre Emacs déjà
ouverte, sinon, on lance beaucoup de processus Emacs, c'est moins
pratique, et ça consomme beaucoup plus de ressources de l'ordinateur.

En pratique, on va beaucoup plus vite en faisant la même chose au
clavier, sans utiliser le menu. Depuis une fenêtre Emacs, faire
`C-x C-f`, c'est-à-dire Control-x puis (sans relâcher la touche Control
pour aller vite), Control-f. Une invite apparaît en bas de la fenêtre
(ce qu'Emacs appelle le « minibuffer »). Entrez le nom du fichier (la
touche tabulation permet de compléter quand on a entré les premières
lettres) et validez avec Entrée :

![](img/emacs/Emacs-control-x-control-f.png "Emacs-control-x-control-f.png")

### Créer un fichier

Quand on lance la commande `emacs` sans argument, une fenêtre s'ouvre,
mais elle ne correspond à aucun fichier. Si on veut travailler sur un
nouveau fichier, il faut créer ce fichier **avant** d'entrer son
contenu (ce qui permet à Emacs de savoir à quel langage de programmation
il doit s'adapter) :

`emacs`

![](img/emacs/Emacs-visit-new-file.png "Emacs-visit-new-file.png")

En fait, tout ce que nous venons de voir pour ouvrir un fichier existant
s'applique aussi pour créer un nouveau fichier : il suffit de demander
à ouvrir un fichier qui n'existe pas, et Emacs le créera pour vous. Par
exemple depuis la ligne de commande :

`emacs nouveau_fichier.py`

(Attention, cette version ouvre une nouvelle instance d'Emacs, à éviter
si vous avez déjà un Emacs lancé)

Le plus rapide si vous avez déjà un Emacs lancé : `C-x C-f` (comme
ci-dessus), entrez le nom du fichier à créer puis validez avec Entree :

![](img/emacs/Emacs-control-x-control-f-new.png "Emacs-control-x-control-f-new.png")

### Naviguer entre les fichiers ouverts

Si vous avez suivi les explications ci-dessus, vous avez maintenant deux
fichiers ouverts dans Emacs : `hello.py` et `nouveau_fichier.py`. Dans
la terminologie Emacs, un fichier ouvert s'appelle un *buffer* (c'est
la copie du contenu du fichier dans la mémoire). On peut passer de l'un
à l'autre avec le menu « Buffers » :

![](img/emacs/Emacs-buffers.png "Emacs-buffers.png")

Là aussi, on peut aller bien plus vite au clavier avec le raccourcis
`C-x b` (Control-x, puis relâcher, puis touche b). Entrez le nom du
buffer (ou laissez vide pour choisir le buffer suggéré par Emacs), puis
validez avec Entrée :

![](img/emacs/Emacs-control-x-b.png "Emacs-control-x-b.png")

Emacs et le Python :snake:
--------------------------

Revenons à notre fichier `hello.py`. Vous avez maintenant une fenêtre
Emacs ouverte, avec le contenu de ce fichier affiché.

### Coloration syntaxique

Premier constat : les mots clés (def, print,…) sont en couleur. Ah,
mais on avait pourtant dit qu'il n'y avait pas de mise en forme dans
un fichier texte ?!? Et non, il n'y a pas de couleurs *dans* le
fichier, mais Emacs nous l'affiche quand même avec des couleurs.
Vérifiez si vous voulez en faisant « `less hello.py` » dans un terminal,
il n'y a pas de couleur.

![](img/emacs/Emacs-coloring-hello-py.png "Emacs-coloring-hello-py.png")

### Indentation

L'indentation (espaces en début de ligne) est cruciale en Python :
c'est ce qui détermine où se trouve la fin d'un bloc de code. Sur
notre exemple, la fonction `dire_bonjour` se termine à la fin du bloc
indenté, elle contient les deux instructions `print`.

Emacs vous aide à indenter votre code correctement : un appui sur la
touche tabulation (notée TAB dans la suite) vous permet de choisir entre
plusieurs indentations pour la ligne courante. Essayez de placer votre
curseur sur la ligne `print("Et bienvenue à l'Ensimag !")`, et appuyez
sur la touche TAB plusieurs fois. Emacs vous propose une version où
cette ligne est désindentée (donc à l'extérieur de la fonction) et la
version d'origine.

![](img/emacs/Emacs-python-indent.png "Emacs-python-indent.png")

Pour indenter/désindenter plusieurs lignes, Emacs vous aide aussi.
Essayez par exemple de répéter 3 fois le contenu de la fonction en
ajoutant `for i in range(3):` autour du corps de la fonction :

-   Ajoutez la ligne `for i in range(3):` en début de fonction
-   Sélectionnez les deux instructions `print` (à la souris)
-   Appuyez sur Control-C, relâchez, puis appuyez sur la touche \> : les
    deux lignes sont décalées. On aurait pu le faire pour un morceau de
    code plus long en utilisant autant de touches.

![](img/emacs/Emacs-python-shift-region.png "Emacs-python-shift-region.png")

On peut faire l'opération inverse avec Control-C puis \< pour
désindenter un morceau de code.

### Exécution d'un programme Python 🚲

Emacs permet d'exécuter un programme Python sans quitter son éditeur,
mais restons didactiques pour le moment en montrant que ce n'est **jamais**
l'éditeur (Emacs, Vim ou VS Code) qui exécute le programme python : ouvrez
un terminal et si besoin placez-vous (`cd`) dans le même répertoire
que le fichier `hello.py`.

On peut exécuter le programme en entrant la commande `python3 hello.py`
(attention, le 3 dans « python3 » est important) dans le terminal :

![](img/emacs/Emacs-term-python.png "Emacs-term-python.png")

Une autre possibilité est de spécifier à l'intérieur du fichier qu'il
s'agit d'un programme Python 3 en ajoutant en première ligne de
`hello.py` :

``` python
#! /usr/bin/env python3
```

Il faut maintenant rendre le fichier exécutable en exécutant cette
commande dans un terminal :

`chmod +x hello.py`

On peut maintenant exécuter le programme avec `./hello.py` :

![](img/emacs/Emacs-term-chmod+x.png "Emacs-term-chmod+x.png")

### Vérifications à la volée 🛫

Un défaut potentiel du langage Python est que la plupart des erreurs de
programmation ne sont levées que pendant l'exécution du programme. Pour
gagner du temps et écrire du code le plus fiable possible, il existe
cependant des outils qui permettent de trouver certaines erreurs avant
d'exécuter, et éventuellement en cours de frappe.

### Utilisation de Flycheck et Flake8

Nous allons commencer avec l'outil `flake8`, qui trouve à la fois de
problèmes de formatage de code et des bugs potentiels comme des
utilisations de variables ou fonctions indéfinis.

Essayons dans un premier temps depuis un terminal : lancez la commande

`flake8 hello.py`

En principe, la commande ne doit produire aucune sortie : `flake8` n'a
trouvé aucun problème avec notre programme. Modifiez maintenant le
programme en remplaçant la définition de la fonction par
`def dire_bonjour(arg)` (remplacer « nom » par « arg ») et recommencez.
Vous devriez obtenir un avertissement :

![](img/emacs/Emacs-term-flake8.png "Emacs-term-flake8.png")

En effet, nous utilisons la variable `nom` qui est maintenant indéfinie
: c'est un bug. Le « F821 » n'est pas important : c'est le numéro de
l'erreur.

Emacs peut nous faciliter la vie en appelant les outils comme `flake8`
pour nous au fur et à mesure qu'on écrit le programme.

Dans la fenêtre Emacs, faites Alt-x (que l'on note `M-x` dans le jargon
Emacs), puis entrez `flycheck-mode`, comme ceci :

![](img/emacs/Emacs-M-x-flycheck-mode.png "Emacs-M-x-flycheck-mode.png")

Validez en appuyant sur « Entrée ». Emacs devrait vous répondre
« flycheck mode enabled » :

![](img/emacs/Emacs-flycheck-mode-enabled.png "Emacs-flycheck-mode-enabled.png")

Si ça ne marche pas, c'est que le mode flycheck n'est pas installé.

La variable `nom` est maintenant soulignée en rouge. En passant la
souris ou en passant le curseur sur l'erreur, on voit apparaître le
message précis (undefined name 'nom'). Il n'y a plus qu'à corriger
(par exemple en revenant au programme d'origine en changeant « arg »
par « nom » dans la déclaration de la fonction).

Pour voir d'un coup d'œil s'il reste des erreurs, la barre en bas
(appelée « modeline » dans Emacs) affiche `FlyC` quand tout va bien, et
`FlyC:N/M` pour dire qu'il reste N erreurs et M avertissements.

### Utilisation de Pylint

Flake8 est content de notre petit programme, mais nous sommes plus
exigeants que cela. Nous allons maintenant dire à Emacs d'utiliser
l'outil Pylint, bien plus pointilleux que flake8.

Ouvrez le fichier `.emacs.d/init.el`, et faites en sorte qu'il contienne ces
lignes :

``` lisp
;; Nécessaire pour pouvoir configurer les packages additionnels
(require 'package)
(package-initialize)

;; Vérifications en cours de frappe
(when (functionp 'global-flycheck-mode)
  (global-flycheck-mode 1)
  (push 'python-pylint flycheck-checkers)
  )
```

Dans le `.emacs.d/init.el` fourni, ces lignes sont déjà présentes pour vous aider.

Rechargez le fichier en faisant `M-x eval-buffer RET` (Alt-x, puis
relacher, puis entrer « eval-buffer », puis validez avec Entrée). On
peut aussi fermer Emacs et le relancer.

Ces quelques lignes font deux choses :

-   Activer le mode flycheck par défaut pour tous les fichiers.
-   Utiliser pylint pour les fichiers Python

Revenez maintenant au fichier `hello.py` et si besoin faites une
modification triviale (ajouter un espace puis le supprimer par exemple)
pour relancer l'analyse par flycheck. C'est maintenant Pylint qui vous
donne les avertissements, et il en trouve trois nouveaux :

![](img/emacs/Emacs-flycheck-pylint.png "Emacs-flycheck-pylint.png")

Nous pouvons corriger comme ceci :

``` python
"""Module Python pour tester Flycheck et Pylint."""

def dire_bonjour(nom):
    """Fonction qui dit bonjour à l'utilisateur."""
    for _ in range(3):
        print("Bonjour,", nom, "!")
        print("Et bienvenue à l'Ensimag !")

dire_bonjour("vous")
```

Les deux chaînes de caractères ajoutées sont des « docstrings », et
permettent de documenter le module python (en haut du fichier) et la
fonction (entre la ligne `def` et la première ligne de code de la
fonction). Nous avons remplacé `i` par `_` qui est la convention Python
pour dire « une variable qui n'est jamais utilisée ».

### Lister les erreurs, naviguer entre les erreurs ⛐

L'affichage des erreurs dans le texte est pratique pour les
éviter/corriger en cours de frappe, mais sous-optimal quand il s'agit
de corriger plusieurs erreurs dans un fichier assez long (ce qui arrive
souvent dans la vraie vie, mais aussi dans certaines étapes du jeu de
piste \…). Une autre option est d'afficher la liste des erreurs. Dans
une fenêtre Emacs, faites `C-c ! l` (c'est-à-dire : Control-c, puis !
puis la lettre l), ou bien `M-x flycheck-list-errors RET` (c'est à dire
Alt-x, puis flycheck-list-errors, puis validation avec la touche Entrée.
La touche tabulation permet de compléter, donc « flycheck-l » puis un
appui sur tabulation suffit). Vous devriez obtenir ceci :

![](img/emacs/Emacs-flycheck-list-errors.png "Emacs-flycheck-list-errors.png")

Vous pouvez maintenant cliquer sur une erreur pour placer le curseur au
bon endroit.

Avec ou sans l'affichage de la liste, on peut aussi passer d'une
erreur à l'autre avec `C-c ! n` (erreur suivante, 'n' comme 'next')
et `C-c ! p` (erreur précédente, 'p' comme 'previous').

Plus d'informations sur [cette
page](http://www.flycheck.org/en/latest/user/error-list.html).

Quelques généralités sur Emacs
------------------------------

### Rechercher une chaine

La recherche de chaine dans Emacs est un peu différente de ce dont vous
avez sans doute l'habitude, mais très pratique :

-   C-s (control-s): Emacs n'ouvre pas de nouvelle fenêtre, mais
    demande la chaine à rechercher dans le « minibuffer » (en bas de la
    fenêtre)
-   On commence à entrer la chaine, Emacs cherche au fur et à mesure
    qu'on tape.
-   Pour aller à l'occurence suivante, C-s à nouveau.

![](img/emacs/Emacs-python-Control-s.png "Emacs-python-Control-s.png")

### Exploration du menu Options

Les fonctionnalités les plus courantes d'Emacs sont disponibles dans le
menu. Regardons le menu `Options`.

Par exemple, sélectionnez `Paren Match Highlighting`. Cette option
active la correspondance visuelle des parenthèses. Essayez de taper, par
exemple, « `print(2 + (3 - (4 + 5)))` ». Quand le curseur est sur une
parenthèse ouvrante, ou derrière une parenthèse fermante, la parenthèse
correspondante se met en couleur.

![](img/emacs/Emacs-python-parent-match.png "Emacs-python-parent-match.png")

Il y en a d'autres. On peux noter en particulier :

-   « `C-x/C-c/C-v Cut and Paste` » (raccourcis claviers usuels sous
    Windows/Mac OS pour le copier/coller) pour être moins dépaysés.

-   `Show/Hide → Speedbar`, pour naviguer dans vos fichiers et vos
    fonctions.

-   `Options → Customize Emacs → Specific Option → “inhibit-splash-screen”`
    : `toggle` pour mettre a on, puis `State → Save for future sessions`
    : Désactiver l'écran d'accueil.

Notez que les options ne s'appliquent qu'au processus Emacs courant.
Pour les sauvegarder pour la prochaine fois, faites `Save options` dans
le même menu.

### Le fichier .emacs.d/init.el

Nous allons maintenant aller un peu plus loin dans la configuration
d'Emacs. Ouvrez le fichier `.emacs.d/init.el` (il peut aussi s'appeler
au choix `.emacs` ou `.emacs.el`, c'est comme on veut), à la racine de
votre compte (menu `File`, `Open File`, ou bien `C-x C-f
~/.emacs.d/init.el RET`, c'est à dire Control-x, puis Control-f, le
nom du fichier .emacs, puis Entrée).

Le début du fichier est votre configuration, telle qu'elle vous est
proposée par l'équipe pédagogique. Nous avons changé le moins de choses
possibles pour que vous ne soyez pas dépaysés si vous avez à utiliser
Emacs sur d'autres machines, mais n'hésitez pas à jouer avec votre
configuration.

À la fin du fichier, vous devriez trouver ce que « Save Options » a fait
si vous avez suivi les instructions quelques lignes plus haut.

### Raccourcis utiles

La refcard Emacs contient un tas de raccourcis utiles. Elle est
disponible ici (`C-x` veut dire « Ctrl-x », `M-y` veut dire « Alt-y »,
`C-x h C-M-\` veut dire « Ctrl-x, puis relâcher, puis h, puis relâcher,
puis Ctrl-Alt-backslash (donc, sur un clavier azerty, Ctrl-Alt-AltGr-8)
» par exemple)
[emacs-refcard.pdf](https://www.gnu.org/software/refcards/pdf/refcard.pdf)

Avec de l'expérience, vous n'aurez plus besoin de votre souris, le
clavier va beaucoup plus vite pour la plupart des tâches : en
utilisation courante, on a les deux mains sur le clavier, les raccourcis
clavier sont à une fraction de seconde de vos doigts…

Par ailleurs savoir utiliser son éditeur de texte entièrement au clavier
est particulièrement utile, quand on se connecte avec ssh : En mode
graphique (ssh -X), l'interface est souvent trop lente pour être
utilisable, et quand on ne dispose pas de l'option graphique (-X ou
-Y), comme avec [Putty](Putty "wikilink"), on ne dispose plus que des
commandes du clavier.

### Aller plus loin

Nous n'avons vu que le strict minimum pour commencer avec Emacs, mais
apprendre *vraiment* à se servir de cet éditeur prend des années. Il est
vivement conseillé de lire une documentation plus complète, comme l'une
proposée dans [la page de suppléments à emacs](emacsplus.md). Vous allez passer
énormément de temps devant votre éditeur de texte au cours de votre
scolarité, vous gagnerez un temps énorme si vous êtes efficaces avec cet
outil.

Un peu d'aide pour le jeu de piste
-----------------------------------

Dans l'étape D3 du jeu de piste, on vous demande de faire
**rapidement** plusieurs corrections sur un fichier pour le compiler.
Voici quelques conseils pour aller vite.

### Décommenter une portion de code

Pour décommenter une portion de code, on peut utiliser
`M-x uncomment-region RET`, c'est à dire faire Alt-x (dans le jargon
Emacs, `M` veut en général dire Alt), relacher, puis entrer
uncomment-region (la complétion marche, vous pouvez écrire juste `unc`
puis appuyer sur la touche tabulation), puis appuyer sur la touche
Entrée.

Pour sélectionner tout le texte, on peut utiliser Menu « Edit » -\> «
Select all », mais le temps presse, on ira beaucoup plus vite au clavier
: `C-x h` (c'est à dire Control-x, relacher, puis 'h').

En résumé : `C-x h M-x unc TAB RET` (10 touches).

(On aurait aussi pu utiliser les [opérations sur les
rectangles](http://www.gnu.org/software/manual/html_node/Rectangles.html)
pour effacer les \# en début de ligne)

### Rechercher-remplacer

Pour remplacer toutes les instances d'une chaine par une autre chaine,
Emacs permet comme tout bon éditeur de texte de faire un
rechercher/remplacer. On peut le trouver dans Menu « Edit » -\> «
Replace » -\> « Replace string », mais là encore, le temps presse et
nous irons beaucoup plus vite au clavier : `M-% [ RET ( RET` (sur un
clavier azerty, vous aurez bien sûr besoin de la touche shift pour
obtenir le %). Emacs positionne le curseur sur la première occurrence de
texte à remplacer, l'utilisateur peut accepter (touche Espace), aller à
l'occurrence suivante (touche 'n'), mais ce qui nous intéresse ici,
c'est de tout remplacer d'un coup : touche '!'.

Emacs parcourt tout le texte de haut en bas, il faut donc démarrer d'en
haut. Pour placer le curseur au début du texte, faites `M-<`.

En résumé : `M-< M-% [ RET ( RET ! M-< M-% ] RET ) RET !` (19 touches
sur un clavier azerty, une petite dizaine de secondes)

### Corriger les erreurs rapidement

Pour exécuter le programme dans un terminal, vous devriez avoir une
commande comme

`python3 text_editor.py`

dans l'historique de votre shell. Pour exécuter, il suffit donc de
faire « flèche haut » (remonter dans l'historique), « Entrée » (2
touches).

En cas d'erreur à l'exécution, on peut utiliser la commande `M-g g`
pour se positionner sur une ligne en particulier (*beaucoup* plus rapide
que de chercher la ligne à tâtons à la souris). Par exemple, si Python
affiche dans votre terminal:

``` console
you@ensipc$ python3 toto.py
    File "toto.py", line 15
    if 0 = 0:
            ^
SyntaxError: invalid syntax
```

Il suffira d'entrer `M-g g 15 RET` (6 touches, environ 3 secondes) pour
vous placer ligne 15.

L'autre option, probablement la plus efficace, est bien sûr d'utiliser
le mode flycheck pour passer d'une erreur à l'autre (cf.
[ci-dessus](#flycheck-list-errors "wikilink")).

### Copier-coller rapide

Une fois le programme compilé et exécuté, on peut faire un copier-coller
du résultat « à la Unix » : sélectionner le texte à la souris (c'est
tout !), puis clic du milieu pour coller.

### Si ça ne suffit pas

Si vous entrez la chaine « moretime » dans le champ qui attend la
réponse, la page vous laissera plus de temps (le temps supplémentaire
est mémorisé d'une fois sur l'autre). Si vous maitrisez bien les
raccourcis claviers, vous ne devriez pas en avoir besoin.

Emacs… les configurations qui brillent dans le noir ✨ {#emacs-config-qui-brillent}
---------------------------------------------------------

Vous pouvez sauvegarder votre configuration courante d'Emacs et
utiliser une autre configuration. Plusieurs rendront jaloux votre
voisin.

`mv -i ~/.emacs.d ~/.emacs.d.old$(date '+%s')`

Vous pourrez ainsi la remettre si besoin.

puis, au choix

  * (maximaliste) Spacemacs (https://www.spacemacs.org)
    `git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d`
	
  * (maximaliste, volontée de ressembler visuellement à CS Code) Doom Emacs (https://github.com/doomemacs/doomemacs)
    `git clone --depth 1 https://github.com/doomemacs/doomemacs ~/.emacs.d`
    `~/.emacs.d/bin/doom install`

  * (beaucoup plus minimaliste) Prelude Emacs (cf https://github.com/bbatsov/prelude pour l'installation) Une partie des idées de la version Ensimag viennent de cette distribution.
