Description
-----------

Vim est un éditeur de code. Son intérêt principal provient de son mode
"commande", qui permet d'effectuer des opérations complexes en un
minimum de temps. Il s'agit également d'un des rares éditeurs de code
- avec Emacs - capable de proposer une édition intelligente (coloration,
indentation, complétion, ...) de la quasi-totalité des langages de
programmation utilisés à l'heure actuelle.

Pourquoi VIM ?
--------------

Vim nécessite un temps d'apprentissage important et un "minimum" de
configuration avant d'être utilisable. Il est légitime de se demander
pourquoi réapprendre à utiliser un éditeur de code (taper `:w` pour
sauvegarder alors qu'on est habitué au CTRL+S peut sembler une perte de
temps, à priori).

La réponse est simple : gagner en efficacité. Il est certains que dans
vos premières semaines d'utilisation de Vim, vous aurez l'impression
de mouliner et de perdre du temps à switcher en permanence entre mode
commande et édition, mais une fois la dizaine de commande de base
connues, à vous le pissage de code productif ! (Des exemples de la
puissance de VIM sont disponibles dans la suite...)

De plus : il est tout à fait possible d'utiliser les commandes
"normales" (CTRL+S, CTRL+C/CTRL+V, ...) dans VIM. Ca n'est bien sûr
pas recommandé, mais c'est bien pratique dans les premières semaines
(mois ?) d'utilisation. Un fichier de configuration `.vimrc`
"n00b-proof" est disponible dans la suite : utilisez le dans vos
premières semaines en tentant de basculer de plus en plus vers les
commandes de base de VIM...

Vim est également l'un des rares éditeurs à fonctionner aussi bien dans
un shell (comme `nano`) que dans une interface graphique avancée (via
`gvim)`, ce qui le rend utilisable dans toutes les situations.

Autres avantages de VIM, en vrac : léger, souris fonctionnelle lors
d'une édition en ligne de commande (contrairement à `nano` ou la plupart
des éditeurs fonctionnant dans un shell), utilisable au sein de la
plupart des grands IDE (eclipse, VS Code, ...), disponible partout (même
quand on pirate un TX en root, c'est dire !), édition de fichier à
distance en ssh, ...

VIM sucks ! Eclipse powwwa !
----------------------------

Je vois d'ici les fans d'Eclipse bondir et me reprocher de faire la
pub d'un éditeur antique, sans fonctions de refactoring avancé, sans
compilation inline, etc. Effectivement, en tant qu'IDE pour des
langages modernes (Java, C#, ...), Vim c'est moyen. Mais, car il y a
un mais : la puissance de VIM ne vient pas du logiciel en lui même, mais
de ses "key bindings" et de son mode commande ! Et il est évidemment
possible d'utiliser ce mode commande dans Eclipse et dans Visual
Studio, avec les plugins adaptés ! Et là c'est le must : non seulement
vous êtes dans un environnement de travail moderne, mais en plus vous
utilisez le rapid-tapage productif de Vim. Apprendre les quelques
commandes de VIM n'est donc pas perdu !

**Plugins pour Eclipse :**

-   [viPlugin](http://satokar.com/viplugin/index.php) : ajoute quelques
    fonctionnalités du mode commande de Vim dans Eclipse. Très incomplet
    (pas de folding (za), ...) et cher (15€). A éviter.
-   [Eclim](http://eclim.sourceforge.net/) : Eclim effectue le travail
    inverse de viPlugin : il apporte les fonctionnalités d'Eclipse à
    Vim ! Autrement dit, vous avez toutes les fonctionnalités de Vim +
    l'intellisense Java + refactoring avancé + ... Le site contient la
    liste détaillée des features et quelques screenshots.

**Plugins pour Visual Studio :**

-   [ViEmu](http://www.viemu.com/) : ajoute la plupart des raccourcis
    Vim à Visual Studio. Le plugin a été pensé pour être le plus fidèle
    possible à Vim. C'est globalement le cas, malgré quelques
    key-bindings manquants (CTRL+A sélectionne tout le code au lieu
    d'incrémenter le nombre suivant dans le document, CTRL+P imprime au
    lieu de faire de la complétion, le fonctionnement de l'undo est
    légèrement différent de celui de Vim... Bref, quelques détails). Le
    plugin est payant (99\$).

**Plugins pour Netbeans :**

-   [jVi](http://jvi.sourceforge.net/) : très complet, très bien intégré
    et très configurable. Rien à redire.

Documentation
-------------

-   [VIM Adventures](http://vim-adventures.com/) un tuto pour apprendre
    vim sous la forme d'un jeu d'aventure. Déplacez le personnage avec
    les mêmes commandes que pour bouger le curseur dans Vim !
-   **The Vim book** de *Steve Oualline* est disponible librement
    [ici, en PDF](https://www.truth.sk/vim/vimbook-OPL.pdf)


Un Vim facile ?
---------------

### Evim

Le mode commande de Vim peut être déroutant au début. Si vous voulez un
éditeur similaire à gedit / nedit mais avec des fonctions avancées,
utilisez la commande "evim". Evim, ou Easy Vim, c'est simplement Vim
sans son mode commande. Vous bénéficiez donc de l'indentation
automatique, de la coloration, de la complétion (CTRL+P), dans un
éditeur qui est aussi facile d'utilisation qu'un bloc-notes (CTRL+S
pour sauvegarder, etc.). Vous pouvez également utiliser le menu de
l'éditeur pour accéder aux fonctions avancées de vim (enregistrement de
macro, etc.) Une fois habitué à evim, tentez de passer à vim avec le
vimrc pour débutant, vous gagnerez en productivité.

### Cream

Dans la même catégorie, on trouve
[Cream](http://cream.sourceforge.net/), une vim pré-configuration pour
fonctionner comme la plupart des éditeurs de textes modernes, ce qui
permet aux débutant d'entrer plus doucement et sûrement dans le bain de
la vim-awesomeness.

.vimrc pour commencer
---------------------

Ce fichier de configuration vous permettra de rendre Vim un peu plus "n00b-proof". En rajoutant ces quelques lignes à votre `.vimrc` (`$HOME/.vimrc` sous linux, `%Program Files%/Vim/_vimrc` sous Windows), vous pourrez utiliser tous les raccourcis classiques et éviter quelques comportements "étranges" de Vim par défaut (Backspace qui supprime pas, ...).

A noter que cette configuration est utilisée par défaut lors d'une installation de GVim sous Windows ! Si vous êtes un pur r0xx0r, pensez à modifier votre `_vimrc` pour supprimer l'utilisation du fichier `mswin.vim`.

Ces modifications sont utiles sous Linux. Ajoutez ces lignes en tête de votre `$HOME/.vimrc` :
```
   set nocompatible
   source $HOME/.vim/mswin.vim
   behave mswin
```

Le fichier `mswin.vim`, à placer dans le dossier `$HOME/.vim` est disponible ici : [mswin.vim](http://vim.cybermirror.org/runtime/mswin.vim).

Les plugins essentiels
----------------------

Avec la section précédente, vous commencez à avoir un éditeur pas trop
mal : il colore bien, il indente bien, ... Ceci dit, il reste encore un
peu obscur. Petite sélection de plugins essentiels, valable pour tous
les languages, pour rendre Vim définitivement meilleur qu'un nedit ou
un [Notepad++](Notepad++ "wikilink") :

-   [minibufexpl](http://vim.sourceforge.net/scripts/script.php?script_id=159)
    : vim possède une navigation par onglet, mais il ne l'utilise pas
    par défaut. Par défaut tous les fichiers ouverts sont chargés dans
    des "buffers" (l'équivalent d'un onglet caché, avec partage des
    registres : par exemple si vous enregistrez une macro, elle sera
    utilisable par tous les fichiers ouverts dans les buffers). Le
    problème c'est que ces buffers n'apparaissent pas à l'écran, il
    est donc difficile de se retrouver dans les fichiers ouverts. Ce
    plugin ajoute une barre d'onglet supplémentaire permettant de
    parcourir tous les buffers : ![](img/vim/VIM01.PNG "fig:VIM01.PNG")

### Plugins par langage

Il existe tout un tas de plugins disponibles sur le site de [vim](http://www.vim.org),
dans la section scripts.

Les color schemes (tout aussi essentiels)
-----------------------------------------

La coloration par défault dans vim, même si elle peut en contenter plus
d'un, peut se révéler ennuyeuse après de longues nuitées passées à
écarquiller les yeux devant un écran, voire insuffisante si vous décidez
de coder dans un langage non ou mal supporté. C'est pourquoi vous
pouvez avoir besoin de trouver de nouvelles colorations. Là, vous avez
l'embarras du choix mais quelqu'un s'est amusé à en regrouper pour
vous (cf. plus bas, une petite sélection)

Les colorscheme de vim sont des fichiers de la forme `*.vim`,
normalement situés dans le dossier `~/.vim/colors`. Pour charger un
nouveau colorscheme dans vim, il suffit de

-   le mettre dans le dossier `~/.vim/colors`
-   ajouter cette ligne à son \~/.vimrc:
    `colorscheme `<nom du colorscheme>

**Une petite selection de colorscheme qui valent la peine**

-   [Les colorschemes les plus
    utilisés](http://github.com/daylerees/colour-schemes): Attention, à
    bien cliquer sur `vim-themes`.

Devenir productif
-----------------

Maintenant vous avez un environnement de travail sympathique. A tout
casser, ça vous a pris 15 minutes et vous avez un environnement qui
correspond à vos premiers désirs. Maintenant il est temps de devenir
efficace, ou du moins de comprendre comment taper un mot, autrement dit
: apprendre les key-bindings de Vim. C'est souvent la partie qui donne
sa mauvaise réputation à Vim et pourtant il n'y a en réalité que 4 ou 5
touches essentielles à apprendre. C'est en combinant ces touches que
vous effectuerez des actions complexes.

Un petit tour sur cette [cheatsheat](https://vim.rtorr.com/) devrait vous être utile.

Vim ... La configuration Ensimag
----------------------------------

Vous pouvez sauvegarder votre configuration courante de Vim et
utiliser la configuration Ensimag conseillée avec les
commandes.

`mv -i ~/.vimrc ~/.vimrc$(date '+%s')`

puis

`cp -ir ~jdpunix/default-config/.vimrc ~/.vimrc`
