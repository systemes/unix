Cette page est écrite pour permettre à des débutants avec l'éditeur de code Spyder5 de bien démarrer, en utilisant le langage Python comme exemple.

# Spyder 5, un environnement de développement python 

Spyder est un [environnement de développement](https://fr.wikipedia.org/wiki/Environnement_de_d%C3%A9veloppement) (ou [IDE](https://en.wikipedia.org/wiki/Integrated_development_environment)) scientifique écrit en python pour le langage Python.

## Installation 

Spyder5 est installé par défaut sur les machines de l'Ensimag. 

Pour l'installer sur vos machines personnelles, il suffit de récupérer le programme d'installation tout-en-un sur <https://www.spyder-ide.org/#section-download>. 
Si besoin, le guide officiel d'installation se trouve ici : <https://docs.spyder-ide.org/current/installation.html>. 

Pour des soucis spécifiques d'OS, nous les rassemblerons ci-dessous : 

- Mac OS
  - *Impossible d'ouvrir Spyder.app car le développeur ne peut pas être vérifié* : `Preferences > sécurité et confidentialité > onglet "General" en bas : Ouvrir quand même`

Le reste du tutoriel sera sur Spyder 5 sur Ubuntu dans l'environnement Ensimag. Rassurez-vous, ce sera très proche de ce que vous aurez sur vos machines personnelles (Windows, Mac, Autre linux)

## 1er lancement 

La figure suivante présente comment lancer Spyder dans Ubuntu. 

![](img/spyder/01_Spyder_Lancement_copie.png)

## L'interface Utilisateur 

*inspiré de <https://docs.spyder-ide.org/current/quickstart.html> et du menu Help > Introduction Tour*

La figure suivante présente l'interface utilisateur de Spyder5 et l'agencement par défaut des différents éléments le constituant. Découvrons ces principaux constituants :  

![](img/spyder/02_Spyder_Layout_copie.png)

Les zones les plus importantes sont :

* **Toolbar (A)** : la barre des Tâches permet un accès rapide aux actions les plus fréquentes comme ouvrir, sauvegarder, exécuter et débogguer un fichier
* **Editor (B)**: L'éditeur qui offre des fonctionnalités pour faciliter l'écriture de code comme la coloration syntaxique, la complétion automatique, la possibilité d'ouvrir plusieurs fichiers, une zone à gauche de numéros de ligne où des avertissements/erreurs peuvent être indiqués, ...
* **IPython Console (C)**: une console permettant de voir le résultat de l'exécution du code soit du code du fichier en cours d'édition soit de commandes directement saisies de manière interactive
* **Variable Explorer (D)**: L'explorateur de variables permet de voir et d'éditer les variables générées lors de l'exécution d'un programme ou de commandes dans la consoles IPython


Pour découvrir rapidement ces zones de manière interactive ainsi que le reste des zones : 

* regarder <https://docs.spyder-ide.org/current/quickstart.html>
* lancer le tour interactif directement dans votre Spyder via le menu `Help > Show Tour`

Pour plus de détails concernant chaque panneau, la [documentation](https://docs.spyder-ide.org/) offre une section "PANES IN DEPTH".

Si la configuration par défaut (taille de police, couleurs, ...) ne vous convient pas, vous pouvez personnaliser l'interface Utilisateur. Vous pouvez, par exemple, changer la langue par défaut du Français vers l'Anglais via `Preferences > Application > Onglet "Advanced Settings" - Language`. En cas de souci avec l'IHM, on trouve généralement dans un moteur de recherche plus de pages pertinentes avec des mots clés en anglais. 

Si vous souhaitez plutôt avoir un spyder avec un arrière-plan noir plutôt que blanc, il suffit de choisir dans `Tools > Preferences > Appearance ` (ou `Spyder > Preferences > Appearance` sous MacOS):

* "Interface": Dark
* "Syntax highlighting theme": Spyder Dark
* et cliquer sur le bouton `Apply`

Partons à la découverte de ces différentes zones sur des exemples de code. 

## La Toolbar 

La Toolbar peut être décomposé en 4 zones. 

![](img/spyder/03_Spyder_Toolbar_copie_2.png)

La zone `FICHIER` contient 4 icônes permettant de : 

* de créer un fichier
* d'ouvrir un fichier existant
* de sauvegarder le fichier en cours d'édition
* de sauvegarder tous les fichiers ouverts dans les onglets de l'éditeur

<!-- La zone `EXECUTER` contient 4 icônes permettant de :
* d'éxécuter le fichier en cours d'édition (`F5`)
* d'exécuter la cellule courante (`Ctrl+Entrée`)
* d'exécuter la cellule courante et de passer à la suivante (`Shift+Entrée`)
* d'exécuter la sélection ou le bloc de lignes (`F9`) -->

Nous verrons les autres zones au fur et à mesure du tutoriel.


## L'interprète Python 

### Evaluation de quelques expressions 

Au lancement de Spyder, la Console IPython apparaît en bas à droite de la
fenêtre. Dans un premier temps, nous ignorerons tout le reste. Vous
pouvez taper des commandes interactives :
~~~pycon
>>> 2+2
>>> 2 + 3 * 4
>>> (2 + 3) * 4
>>> 10 / 3
>>> 10 // 3
>>> 10 % 3
~~~
### Utilisation de variables 

Évaluez maintenant les expressions suivantes (dans cet ordre) :
~~~pycon
>>> x = 42
>>> y = x + 1
>>> x
>>> y
~~~

Vous pouvez remarquer que, dans l'onglet "Variable Explorer" au dessus de la console, les variables créées apparaissent avec leur valeur à la dernière exécution.


## L'éditeur 

Au lancement, l'éditeur de texte (partie gauche de la fenêtre) est
ouvert sur un fichier Python vide. Il est fortement déconseillé
d'utiliser ce fichier ouvert par défaut : il se trouve caché dans un
répertoire spécifique à Spyder, vous risquez de ne pas le retrouver si
vous y mettez des choses importantes.

La première action à faire après le lancement de Spyder est donc de
créer un nouveau fichier (menu « Fichier », « Nouveau Fichier ») et de
l'enregistrer pour lui donner un nom.

Il y a également la possibilité d'ouvrir un fichier .py directement
avec Spyder depuis l'explorateur (Windows) ou le Finder (Mac OS X)

Entrez dans l'éditeur le programme suivant (vu à un semestre passé) et sauvegardez le sous le nom `fibonacci.py`:
~~~python title="fibonacci.py"
a = 0
b = 1
while b < 10:
    print(b)
    c = a + b
    a = b
    b = c
~~~
Revenons rapidement sur la notion d'"Éditeur de texte". Vous avez sûrement déjà utilisé un "*Traitement* de texte", qui permet de taper du texte, de le mettre en forme (gras, italique, taille et choix des polices, …). Un éditeur de texte édite du texte brut : pas de mise en forme, pas de dessins, juste du texte. Une suite de caractères, quoi. Mais en informatique, on trouve du texte brut partout : langage de programmation, fichiers de configuration de beaucoup de logiciels, …

On pourrait essayer d'éditer le texte d'un programme Python avec un traitement de texte classique, mais on aurait énormément de fonctionnalités inutiles, et par contre, il manquerait énormément de choses. L'éditeur de Spyder fournit toutes ces fonctionnalités indispensables pour écrire du code Python et nous allons aborder ces différentes fonctionnalités dans la suite de ce tutoriel.

### Coloration syntaxique 

Premier constat : les mots-clés du langage (while, print, …) sont en couleur. Ah, mais on avait pourtant dit qu'il n'y avait pas de mise en forme dans un fichier texte ?!? Et non, il n'y a pas de couleurs *dans* le fichier, mais Spyder nous l'affiche quand même avec des couleurs. 

### Indentation 

L'indentation (espaces en début de ligne) est cruciale en Python : c'est ce qui détermine où se trouve la fin d'un bloc de code. Sur notre exemple, le code de la boucle `while` se termine à la fin du bloc indenté. Elle commence par `print(b)` et finit par `b=c`.

Spyder vous aide à indenter/désindenter votre code python. Pour indenter une ligne de code il suffit de placer le curseur au début de la ligne et d'appuyer sur la touche de tabulation (`TAB`). Pour effectuer l'opération inverse, il suffit d'appuyer sur `Shift + TAB`.

Notez que vous pouvez déplacer le curseur rapidement à l'aide du clavier. Par exemple vous pouvez vous déplacer de mots en mots avec les combinaisons `Ctrl + Fleche Gauche/Droite`. Vous pouvez aussi aller aux début et fin de lignes en utilisant les touches Début et fin de lignes (flèches en diagonales vers le haut ou le bas).

Pour indenter/désindenter plusieurs lignes, Spyder vous aide aussi. Il vous suffit de sélectionner les lignes que vous souhaitez indenter et d'appuyer sur la touche `TAB` pour indenter ou `Shift + TAB` pour désindenter (ou via le menu `Edit > Comment/Uncomment`).

<!-- ![](img/spyder/Atom-indentation.png) -->

*De la même manière, si vous voulez sélectionner plusieurs lignes de codes au clavier, vous pouvez utiliser la combinaison `Shift + Fleche Haut/Bas`.*

Vous avez également la possibilité de déplacer a une ligne (ou sélection de lignes) de code vers le haut ou le bas en utilisant la combinaison `Alt + Fleche Haut/Bas`.

### Commentaires  

Voir la section [les commentaires](#les-commentaires).

### Complétion et Snippets  

Spyder embarque un plugin [Kite](https://help.kite.com/category/89-spyder-integration) qui vous fait des suggestions au fur et à mesure que vous saisissez du code. 

Par exemple si dans votre fichier, vous commencer à taper `for` une boite de dialogue dans laquelle il y a cinq suggestions :

* ->| for 					 (keyword)  : détails sur le mot-clé "for"
* ->| format(o, format_spec) (function) : propose la fonction format, sa définition et ses paramètres
* ->| format 				 (keyword)  : remplace `for` par `format`
* ->| for (range) 			 (snippet)  : remplace `for` par la snippet associée
* ->| for (iterator)		 (snippet)  : remplace `for` par la snippet associée

![](img/spyder/04_Spyder_Completion.png)

Si vous choisissez le snippet `for (range)`, un squelette de code (snippet) vous est proposé automatiquement pour la boucle `for` :
~~~python
for i in range(0, n):
    pass
~~~
Cela peut-être pratique si vous oubliez la syntaxe associé à un mot-clé en particulier ou les paramètres d'une fonction.

Si vous tapez `import num`, le module `numpy` vous est proposé automatiquement. 

<!-- Preferences > Completion and linting - Onglet "General"
* Snippets

désactiver kite ? https://www.spyder-ide.org/blog/spyder-kite-funding/
https://www.kite.com/integrations/spyder/ -->
### Exécution 

Pour l'exécuter, tapez la touche F5 (en haut du clavier). Spyder vous
proposera d'enregistrer le fichier, faites-le et si ce n'est pas déjà
fait, donnez-lui un nom explicite, par exemple fibonacci.py.

Vous devriez voir apparaître dans le panneau « Console » (en bas à droite
de la fenêtre Spyder) quelque chose comme :
~~~pycon
In [1]: runfile('fibonacci.py', wdir=r'/tmp')
1
1
2
3
5
8
In [2]: 
~~~

`runfile('fibonacci.py', wdir=r'/tmp')` indique que le résultat sous-jancent est le résultat de l'exécution du fichier *fibonacci.py* dans le répertoire de travail `/tmp`. cela peut être important si le fichier a des dépendances à d'autres fichiers.

Essayez d'ajouter une erreur dans ce programme, par exemple remplacer
`print(b)` par `print(b` (suppression de la
parenthèse fermante).

Enregistrez le fichier. Spyder vous signale une erreur avec une croix rouge
dans la marge à la ligne sous le `print`.

On peut tout de même tenter une exécution : Spyder vous signale alors
une erreur de syntaxe dans le panneau « Console ». En cliquant sur la partie du
message d'erreur avec la ligne correspondante (`  File "chemin_vers_fichier/fibonacci.py", line 12`), le curseur se positionne à la bonne ligne du fichier
concerné. Corrigez l'erreur (la parenthèse fermante manquante du
`print`) et exécutez le programme.

Avec le bouton ![](img/spyder/Spyder_icone_exec_4.png){: style="height:30px"} (`F9`), vous avez également la possibilité de n'exécuter que les lignes sélectionnées (ou la ligne courante si plusieurs lignes ne sont pas sélectionnées). Tester en sélectionnant les lignes suivantes : 
~~~python
print(b)
c = a + b
a = b
b = c
~~~
Vous obtenez alors le prochain nombre entier de la suite de fibonacci qui est 13. 

## Rappels et compléments sur les bases de Python 

### Affichage: print 

!!! example "**Entrées sorties avec `print` et `input`**"

    Créez un nouveau fichier dans l'éditeur de texte de Spyder (menu « Fichier », « Nouveau Fichier »), et entrez le programme suivant :
    ~~~python
    print("Bonjour, quelle est ton année de naissance ?")
    annee = int(input()) #Saisissez votre année de naissance dans la console IPython
    print("Tu as", 2021 - annee, "ans.")
    ~~~

    Exécutez ce programme et saisissez votre année de naissance dans la console IPython.
    Essayez avec autre chose qu'un nombre entier. Vous obtiendrez un message d'erreur du type

        ValueError: invalid literal for int() with base 10

!!! example "**Importance `print`**"

    Essayez d'entrer les expressions ou instructions suivantes dans
    l'interprète interactif :
    
    ~~~pycon
    >>> "Bonjour"
    >>> print("Bonjour")
    >>> 2 + 2
    >>> print(2 + 2)
    ~~~

    Essayez maintenant d'entrer ces 4 lignes dans un programme, dans la fenêtre de l'éditeur de texte. Exécutez ce programme ou uniquement ces lignes. Que  voyez-vous ? Pourquoi ?

### Les commentaires 

Un commentaire est une portion du programme qui est ignorée par
Python. Les commentaires sont là pour aider la relecture du programme
par un être humain.

En Python, un commentaire commence par le caractère `#` et va
jusqu'à la fin de la ligne.


!!! example "**Exercice** : Commentez le programme précédent de la manière suivante"

    ~~~python
    # Cette ligne n'affiche rien :
    "Bonjour"

    print("Bonjour")  * Encore un commentaire
    2 + 2
    print(2 + 2)

    # Commentaire
    # sur plusieurs 
    # lignes.
    ~~~

Pour commenter plusieurs lignes, il suffit d'écrire votre commentaire sur plusieurs lignes sans vous soucier du caractère `#`, de les sélectionner et de les commenter (`Edit > Comment/Uncomment`)

Vérifiez en exécutant le programme que son comportement n'a pas changé.

### Les erreurs 

Les programmeurs sont des êtres humains comme les autres, et ils ont
droit à l'erreur ! Plusieurs types d'erreurs peuvent se produire :

* Les **erreurs de syntaxe**, qui correspondent à des programmes qui ne sont pas *grammaticalement corrects*. Dans ces cas, Python refuse de commencer l'exécution du programme (il ne comprend pas ce qu'on lui demande).
* Les **erreurs à l'exécution** peuvent arriver après le début de l'exécution du programme (Python comprend ce qu'on lui demande, mais il y a un problème lors de l'exécution du programme).

!!! example "**Erreur de syntaxe** : Essayez d'entrer le programme suivant (dans l'interprète interactif ou dans l'éditeur de texte) :"

    ~~~python
    print("Début du programme")
    x = 2 +
    print("Fin du programme")
    ~~~
  
    *Essayez d'exécuter le programme. Que se passe-t-il ?*
    ~~~pycon
      File "/private/tmp/test2.py", line 9
        x = 2 +
               ^
    SyntaxError: invalid syntax
    ~~~

    Nous avons affaire à une erreur de syntaxe indiquée dans la marge de l'éditeur par une croix rouge avant même l'exécution. Le programme ne peut pas s'exécuter.


!!! example "**Erreur d'exécution**:  Modifiez le programme ci-dessus comme ceci :"
    ~~~python
    print("Début du programme")
    x = 1 / 0
    print("Fin du programme")
    ~~~

    *Essayez d'exécuter le programme. Que se passe-t-il ?*

    ~~~pycon
    Début du programme
    Traceback (most recent call last):

      File "/private/tmp/test2.py", line 9, in <module>
        x = 1 / 0

    ZeroDivisionError: division by zero
    ~~~

    Pas de problème de syntaxe. Le programme commence à s'exécuter d'où l'affichage de ` Début du programme` mais une erreur d'exécution (division par zéro) l'interrompt

En Python, les vérifications d'existence et de types des variables
sont faites à l'exécution. Les erreurs suivantes sont donc des erreurs
à l'exécution :
~~~ pycon
>>> x = 'Bonjour' + 42  
Can't convert 'int' object to str implicitly
>>> x = y + z  
name 'y' is not defined
~~~
(D'autres langages comme C ou Java font ces deux vérifications au même
moment que les vérification de syntaxe)

## Débogage avec Spyder 

Soit le programme suivant : 
~~~python
x = 4
y = 5

print("Avant multiplication : x =", x, "et y =", y)

resultat = 0
while y > 0:
    resultat = resultat + x
    y = y - 1
    print("Dans la boucle, resultat =", resultat)

print("Résultat :", resultat)
~~~

L'instruction `print` est utilisée pour observer l'exécution du programme mais il peut devenir vite fastidieux de rajouter un print à chaque fois qu'on veut connaître l'état d'une variable. Une autre
méthode, plus propre, est d'utiliser un débogueur, qui permet
d'observer une exécution du programme sans le modifier.


### L'interface du débogueur 

Pour activer le débogueur, choisissez menu « Déboguer », « Déboguer »,
ou bien cliquez sur le bouton ![](img/spyder/Spyder_icone_debug_1.png){: style="height:30px"} dans la barre de boutons
(« Déboguer le script » en forme de bouton « play/pause », ou
Control+F5). L'exécution est prête, la console affiche un nouveau prompt `IPdb [1]:` et indique que le débogueur est à la première ligne exécutable du programme mais rien n'a encore été exécuté :
à nous d'exécuter les instructions une par une.

La barre de boutons du haut contient entre autres les actions:

* ![](img/spyder/Spyder_icone_debug_2.png){: style="height:30px"} « Run current line » (« Exécuter la ligne en cours ») pour exécuter la ligne courante sans entrer dans une fonction
* ![](img/spyder/Spyder_icone_debug_3.png){: style="height:30px"} « Step into function ... » (« Avancer dans la fonction ... ») pour exécuter la ligne courante. Si l'instruction courante est un appel de fonction, le débogueur « entre » dans la fonction et arrête l'exécution à la première ligne de la fonction
* ![](img/spyder/Spyder_icone_debug_4.png){: style="height:30px"} « Run until current function or method returns » (« Exécuter jusqu'au retour de la fonction ou méthode ») pour exécuter jusqu'à sortir de la fonction courante (très utile si on est entré par erreur dans une fonction)
* ![](img/spyder/Spyder_icone_debug_5.png){: style="height:30px"} « Continue execution until next breakpoint » exécute toutes les lignes jusqu'au prochain point d'arrêt
* ![](img/spyder/Spyder_icone_debug_6.png){: style="height:30px"} « Stop debugging ». Sort du mode débogage. La console affiche à nouveau un prompt de type `In [X]:`

En choisissant l'onglet « Explorateur de variables » du panneau en
haut à droite de la fenêtre Spyder, on peut voir les variables du
programme, leur type et leur valeur au fur et mesure de l'exécution

### Exécution pas à pas 
!!! example "**Exercice: Déboguage**" 

    Exécutez pas à pas le programme avec « Exécuter la ligne en cours », jusqu'à sa fin, et observez les changements des valeurs de `x`, `y` et `resultat`.

C'est quand même plus efficace que mettre des print partout. 

!!! example "**Exercice**"
    Une alternative intéressante : Python Tutor. Ouvrez la page <http://pythontutor.com/>, cliquez sur « Start writing and visualizing code now! » selectionnez Python 3, et entrez un programme dans le champ texte.
    
    Le bouton « Visualize execution » permet d'exécuter le programme pas à pas avec une visualisation graphique des valeurs de variables.
    
Si vous ne voyez pas l'intérêt de Python Tutor pour l'instant, vous le verrez sans doute quand nous verrons les listes en Python.

### Les points d'arrêt 

L'exécution pas à pas peut devenir rapidement énervante si votre programme est grand et que vous n'êtes intéressés par l'état de vos variables à un endroit particulier du programme. D'où l'intérêt des breakpoints (point d'arrêt) !

Reprenons le même code que précédemment et imaginons que vous souhaitiez que le débogueur arrête l'exécution à chaque fois que la variable `resultat` est mis à jour dans la boucle `while`. Pour ce faire, il vous suffit de cliquer à droite du numéro de la ligne de l'instruction `print("Dans la boucle, resultat =", resultat)` et un point rouge fera son apparition. Vous venez de mettre un point d'arrêt !

Ensuite il ne vous reste plus qu'à déboguer le fichier et de cliquer sur ![](img/spyder/Spyder_icone_debug_4.png){: style="height:30px"} pour voir comment évolue la variable resultat à chaque tour de boucle `while`. Le programme sort automatiquement du mode débogage en arrivant à la fin du programme

![Spyder_debug.gif](img/spyder/Spyder_debug.gif)

## Programme multi-fichiers 

Quand on écrit des fonctions qui sont destinées à être utilisées dans
plusieurs programmes, ou lorsqu'on veut séparer les parties d'un
programme (par exemple la partie qui fait le calcul, la partie qui
fait l'affichage, et la partie qui fait le test), on peut séparer le
programme en plusieurs fichiers .py: les *modules* qui ne
contiennent que des fonctions, et les *programmes* qui utilisent
les modules.

Reprenons l'exemple précédent et découpons le en 2 fichiers que nous mettrons dans le même dossier : 

* *multiplication.py* : le module qui va contenir la fonction multiplication
* *test_multiplication.py* qui va utiliser le module *multiplication*

~~~python title="multiplication.py"
def multiplication(argument, facteur_multiplicatif):
    resultat = 0
    while facteur_multiplicatif > 0:
        resultat = resultat + argument
        facteur_multiplicatif = facteur_multiplicatif - 1
        print("Dans la boucle, resultat =", resultat)
    return resultat
~~~
 
~~~python title="test_multiplication.py"
import multiplication as mult

x = 4
y = 5

print("Avant multiplication : x =", x, "et y =", y)
resultat = mult.multiplication(x, y)
print("Résultat :", resultat)
~~~
`import multiplication as mult` va charger le module *multiplication.py* et donc exécuter son contenu créant ainsi la fonction `multiplication` avant d'exécuter le reste du programme. Attention à ce que le fichier *multiplication.py* soit bien dans le même répertoire que le fichier *test_multiplication.py* sinon python sera dans l'incapacité de trouver le module débouchant sur l'erreur ModuleNotFoundError : 

~~~pycon
runfile('/private/tmp/test/multiplication_test.py', wdir='/private/tmp/test')
Traceback (most recent call last):

  File "/private/tmp/test/multiplication_test.py", line 1, in <module>
    import multiplication

ModuleNotFoundError: No module named 'multiplication'
~~~

En fait, Python est fourni avec un ensemble de modules standards (appelé la *bibliothèque standard*), qui
contiennent déjà beaucoup de fonctions. Par exemple, le module
`math` contient une fonction `sqrt` (square root)
que l'on peut utiliser au lieu de `** 0.5` pour calculer une
racine carrée. Pour l'utiliser, il faut d'abord écrire (en début de
programme) `import math`, puis appeler la fonction
`math.sqrt`. 

Ces modules standard ne se trouvent pas à l'évidence dans le répertoire courant. En fait, les modules seront recherchées par défaut dans : 

* les répertoires de `sys.path`. Pour avoir accès à cette variable prédéfinie dans python, il suffit de taper dans la console `import sys` suivi de `sys.path`
* les répertoires définis par l'utilisateur via la variable PYTHONPATH : Menu `Tools > PYTHONPATH Manager` ou l'icône ![](img/spyder/Spyder_icone_PYTHONPATH.png) de la Toolbar
* le répertoire du fichier en cours d'exécution : Cela correspond à notre exemple ci-dessus. Cette option est paramétrée dans `Tools > Preferences > Run - Working directory settings`


<!-- # Fonctionnalités avancées 

## Execution de selection de code ou par bloc ou par cellule  

En dehors de l'exécution de l'ensemble du fichier avec le bouton ![](img/spyder/Spyder_icone_exec_1.png), vous avez d'autre possibilités d'exécution :
* d'exécuter la ligne courante ou les lignes sélectionnés avec ![](img/spyder/Spyder_icone_exec_4.png) (`F9`)
* d'exécuter la cellule courante avec ![](img/spyder/Spyder_icone_exec_2.png) (`Ctrl+Entrée`)
* d'exécuter la cellule courante et de passer à la suivante avec ![](img/spyder/Spyder_icone_exec_3.png) (`Shift+Entrée`)

**Exécution sélection**
Ainsi, après l'exécution du fichier `fibonacci.py`, vous souhaitez exécuter uniquement la partie suivante :

 print(b)
 c = a + b
 a = b
 b = c

Sélectionnez cette partie et cliquez sur ![](img/spyder/Spyder_icone_exec_4.png).
Vous obtenez alors le prochain nombre entier de la suite de fibonacci qui est 13.

**cellules**


Les cellules sont utilisés pour ... et la vue vous offrent une vision ..


Vous pouvez par exe ne souhaitez qu'exécuter qu'une partie du code de votre suite de fibonacci



https://docs.spyder-ide.org/current/panes/editor.html?highlight=block#defining-code-cells


----

- Ouverture d'un projet : https://docs.spyder-ide.org/current/panes/projects.html
- commentaire : https://chamilo.grenoble-inp.fr/courses/ENSIMAG3MMSALG/document/seance01.html
	- en haut du programme
	- en bas d'une fonction
	- ...
- Consoles > New Console
- repartir d'un console neuveet de variable neuves : clic-droit restart kernel
- Reloaded modules: mult
- comment faire/inclure des tests propres et main
- ... -->

