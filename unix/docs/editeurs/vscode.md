Cette page est écrite pour permettre à des débutants avec l'éditeur de
code Visual Studio Code de bien démarrer, en utilisant le langage Python
comme exemple. Une documentation similaire est disponible pour Emacs :
[Premiers pas avec Emacs et
Python](emacs.md).

Visual Studio Code ou VS code ... un éditeur de code
-----------------------------------------------------

[Visual Studio Code (ou VS Code)](https://code.visualstudio.com/) est un
éditeur de code léger, gratuit et multiplateforme développé par
Microsoft.

Sa popularité ([Sondage Stack Overflow
2023](https://survey.stackoverflow.co/2023/#most-popular-technologies-new-collab-tools)) est
allée grandissante ces dernières années du fait de sa modernité et de la
possibilité pour sa communauté de proposer des extensions facilement.

D'abord, qu'est-ce que ça veut dire, un "Éditeur de texte" ? Vous
avez sûrement déjà utilisé un "*Traitement* de texte", qui permet de
taper du texte, de le mettre en forme (gras, italique, taille et choix
des polices, ...). Un éditeur de texte édite du texte brut : pas de mise
en forme, pas de dessins, juste du texte. Une suite de caractères, quoi.
Mais en informatique, on trouve du texte brut partout : langage de
programmation, fichiers de configuration de beaucoup de logiciels, ...

On pourrait essayer d'éditer le texte d'un programme Python avec un
traitement de texte classique, mais on aurait énormément de
fonctionnalités inutiles, et par contre, il manquerait énormément de
choses. Nous allons aborder ces différentes fonctionnalités dans la
suite de ce tutoriel.

### Configuration initiale

Pour lancer VS Code :

-   dans l'invite de commande, tapez `code`
-   ou sinon lancez le depuis l'interface graphique d'Ubuntu
    -   Clic sur "Activites" en haut à gauche
    -   dans la barre de recherche qui fait son apparition en haut,
        tapez "Visual"
    -   clic-droit sur l'icône du logiciel et "Add to Favorites" pour
        l'ajouter dans la barre de vos favoris à gauche

Au premier lancement de VS Code, vous serez invité à une première
configuration de votre éditeur ("Get started with VS Code"). Tout cela
se passe dans un onglet "Welcome" accessible via le menu
`Help -> Welcome` (au cas où il serait fermé par inadvertance).

![Get started with VS
Code](img/vscode/VScode-gettingstarted.png "Get started with VS Code")

4 étapes vous sont proposées:

1.  son apparence : le thème "Dark" est celui par défaut
2.  les langages supportées : commençons par installer le support du
    langage python dans la barre d'extension qui fait son apparition
    une fois cette étape sélectionnée ![extension
    python](img/vscode/VScode-python_annote.png "fig:extension python")
3.  l'accès à la *Command Palette* via le raccourci `Ctrl+Shift+P` qui
    vous permet d'accomplir n'importe quelle tâche via votre clavier
    -   Essayez par exemple de changer la langue de l'IHM en français.
        -   dans la *Command Palette*, tapez *display*
        -   la commande *Configure Display Language* est proposée. il ne
            reste plus qu'à valider (clic-gauche ou la touche
            "Entrée").
        -   choisissez *Install Additional Languages*, sélectionner le
            français dans la barre des extensions qui a fait son
            apparition et installer l'extension
        -   dans la *Command Palette*, relancez *Configure Display
            Language* et sélectionnez "fr" à la place de "en" pour
            english (l'anglais est la langue par défaut de l'IHM)
        -   Les menus de l'IHM sont maintenant en français
        -   Remettez l'IHM en anglais (en cas de bug IHM, vous aurez
            plus de résultats dans votre moteur de recherche en anglais)
4.  l'ouverture d'un premier fichier ou dossier

Références : <https://code.visualstudio.com/docs/getstarted/locales>

### L'interface Utilisateur

La figure suivante présente l'agencement par défaut de VS Code.

![](img/vscode/VSCode-IHM_copie.png "VSCode-IHM_copie.png")

VS Code est divisé en 5 zones :

-   **L'éditeur (Zone C)** : la zone principale permettant l'édition
    des fichiers. On peut ouvrir autant d'éditeurs que possibles et les
    disposer côte à côte soit verticalement soit horizontalement soit
    dans différents onglets. Afin de naviguer plus rapidement dans les
    longs fichiers, une "minimap" donne une vue d'ensemble du fichier
-   **Side Bar (B)** : contient différentes vues comme le navigateur de
    fichiers dans la figure précédente
-   **Status Bar (E)** : des informations compactes sur le projet ouvert
    et les fichiers en cours d'édition
-   **Activity Bar (A)** : permet de basculer entre les différentes vues
    comme le navigateur de fichiers
    ![](img/vscode/VSCode-icone-file.png "fig:VSCode-icone-file.png"), la gestion
    des extensions
    ![](img/vscode/VSCode-icone-extension.png "fig:VSCode-icone-extension.png") ou
    les réglages
    ![](img/vscode/VSCode-icone-reglage.png "fig:VSCode-icone-reglage.png")
-   **Panels (D)** : Différents panneaux peuvent être affichés sous la
    région d'édition afin de donner de plus amples informations
    concernant l'exécution de programme, le débogage, les
    erreurs/warnings ou un terminal intégré

Référence : <https://code.visualstudio.com/docs/getstarted/userinterface>

### Ouvrir un fichier

Vous devriez avoir un fichier `hello.py` dans le répertoire `stage-unix`
sur votre compte. Si ce n'est pas le cas, commencez par télécharger le
fichier
[hello.py](https://chamilo.grenoble-inp.fr/courses/ENSIMAG3MMUNIX/document/stage-unix/hello.py),
et sauvegardez-le sur votre compte.

À partir de l'invite de commande vous pouvez ouvrir le fichier
précédemment téléchargé en entrant la commande suivante :

`code hello.py`

Vous pouvez aussi lancer VS Code et ouvrir un fichier depuis le menu
`File -> Open File...` et sélectionner le fichier souhaité.

Si vous avez plusieurs fichier à ouvrir, vous pouvez répétez ces
opérations à volonté, les fichiers étant alors ajoutés en tant
qu'onglets à VS Code. Cependant, si vous voulez ouvrir l'ensemble des
fichiers situé dans un répertoire, vous pouvez lancer la commande
`code /chemin/vers/mon/repertoire`. Par exemple pour ouvrir l'ensemble
des fichiers présents dans le répertoire courant on entrera :

`stage-unix$ code .`

Une fenêtre vous demandant "Do you trust the authors of the file in
this folder ?" va apparaitre. Cliquez sur le bouton "Yes" afin que
l'ouverture des fichiers de ce répertoire `stage-unix` se fasse dans le
mode "non restricted". Il sera intéressant d'ajouter votre répertoire
racine `~login` aux Trusted Folders (répertoires de confiance). Plus de
détails à ce sujet dans la section Workspace
Trust à la fin de cette page.

Ouvrir un répertoire peut aussi être effectué dans en utilisant la
méthode graphique. Il faut néanmoins noter deux choses :

-   Une ouverture avec `code .` ouvre automatiquement un bandeau à
    gauche de l'éditeur avec l'arborescence des fichiers depuis
    l'endroit ouvert.
-   On lance autant d'instances de VS Code que de dossier ouvert de
    cette manière. À utiliser de manière intelligente donc.

![](img/vscode/VScode-bandeau_copie.png "VScode-bandeau_copie.png")

Il est alors possible de rapidement ouvrir des fichiers contenus dans le
répertoire à l'aide du bandeau de gauche. Il est toujours possible de
masquer ce bandeau via le menu `View -> Appearance -> Show Side Bar` ou
via le raccourci `Ctrl+B` ou en cliquant directement sur l'icône
![](img/vscode/VSCode-icone-file.png "fig:VSCode-icone-file.png") de la barre
d'activité.

Plus d'options concernant le lancement de VS Code à la ligne de
commande au
<https://code.visualstudio.com/docs/getstarted/tips-and-tricks#_command-line>.

### Créer un fichier

Afin de créer un nouveau fichier à partir de l'invite de commande, vous
pouvez entrer la commande suivante
`code /chemin/vers/mon/fichier.extension`. Lorsqu'un fichier est créé
de cette manière, VS Code interprète l'extension afin d'adopter son
comportement. Ainsi si vous faites :

`code nouveau_fichier.py`

VS Code va se lancer (ou ouvrir un nouvel onglet) en sachant qu'il
s'agit d'un fichier python. Vous aurez donc la bonne coloration
syntaxique, les bonne indentations, etc.

![](img/vscode/VSCode-extension_copie.png "VSCode-extension_copie.png")

Depuis une instance de VS Code vous pouvez créer un nouveau fichier en
faisant `File -> New File` ou `Ctrl + N` ou encore double cliquer sur un
endroit vide de la barre d'onglets. Vous remarquerez cependant qu'en
utilisant cette méthode, VS Code ouvre un fichier nommé `Untitled` sans
extension. Il est donc impossible pour VS Code de déterminer le type de
fichier dont il s'agit. Vous n'aurez donc pas de coloration syntaxique
et autres fioritures tant que vous n'aurez pas sauvegardé le fichier
(`Ctrl + S`).

### Naviguer entre les fichiers ouverts

Comme énoncé plus haut, lorsque plusieurs fichiers sont ouverts dans
l'éditeur, VS Code les place dans des onglets (à la manière d'un
navigateur web) au-dessus de la zone d'édition. Si par exemple vous
avez ouvert à la fois `hello.py` et `nouveau_fichier.py`, vous pouvez
passer de l'édition d'un fichier à l'autre en sélectionnant l'onglet
correspondant.

Cela dit, comme pour un navigateur web, vous pouvez naviguer entre les
onglets en utilisant des raccourcis clavier (ce qui peut vous faire
gagner du temps lorsque vous travaillez en alternance sur plusieurs
fichiers). Sur VS Code, cela est réalisable grâce à la combinaison
`Ctrl + TAB`

VS Code et le Python
--------------------

Revenons à notre fichier `hello.py`. Vous avez maintenant une fenêtre VS
Code ouverte avec le contenu de ce fichier affiché.

### Coloration syntaxique

Premier constat : les mots-clés (def, print, ...) sont en couleur. Ah,
mais on avait pourtant dit qu'il n'y avait pas de mise en forme dans
un fichier texte ?!? Et non, il n'y a pas de couleurs *dans* le
fichier, mais VS Code nous l'affiche quand même avec des couleurs.
Vérifiez si vous voulez en faisant « `less hello.py` » dans un terminal,
il n'y a pas de couleur.

![](img/vscode/VSCode-syntax-color.png "VSCode-syntax-color.png")

### Indentation

L'indentation (espaces en début de ligne) est cruciale en Python :
c'est ce qui détermine où se trouve la fin d'un bloc de code. Sur
notre exemple, la fonction `dire_bonjour` se termine à la fin du bloc
indenté, elle contient les deux instructions `print`.

VS Code vous aide à indenter/désindenter votre code python. Pour
indenter une ligne de code il suffit de placer le curseur au début de la
ligne et d'appuyer sur la touche de tabulation (`TAB`). Pour effectuer
l'opération inverse, il suffit d'appuyer sur `Shift + TAB`.

*Notez que vous pouvez déplacer le curseur rapidement à l'aide du
clavier. Par exemple vous pouvez vous déplacer de mots en mots avec les
combinaisons `Ctrl + Fleche Gauche/Droite`. Vous pouvez aussi aller aux
début et fin de lignes en utilisant les touches Début et fin de lignes
(flèches en diagonales vers le haut ou le bas).*

Pour indenter/désindenter plusieurs lignes, VS Code vous aide aussi. Il
vous suffit de sélectionner les lignes que vous souhaitez indenter et
d'appuyer sur la touche `TAB` pour indenter ou `Shift + TAB` pour
désindenter.

*De la même manière, si vous voulez sélectionner plusieurs lignes de
codes au clavier, vous pouvez utiliser la combinaison
`Shift + Fleche Haut/Bas`.*

### Exécution d'un programme Python

VS Code permet d'exécuter un programme Python sans quitter son éditeur,
mais restons simple pour le moment : ouvrez un terminal et si besoin
placez-vous (`cd`) dans le même répertoire que le fichier `hello.py`.

On peut exécuter le programme en entrant la commande `python3 hello.py`
(attention, le 3 dans « python3 » est important) dans le terminal :

Une autre possibilité est de spécifier à l'intérieur du fichier qu'il
s'agit d'un programme Python 3 en ajoutant en première ligne de
`hello.py` :

`#! /usr/bin/env python3`

Il faut maintenant rendre le fichier exécutable en exécutant cette
commande dans un terminal :

`chmod +x hello.py`

On peut maintenant exécuter le programme avec `./hello.py` :

![](img/vscode/VSCode-term-chmod.png "VSCode-term-chmod.png")

### Vérifications à la volée

Un défaut potentiel du langage Python est que la plupart des erreurs de
programmation ne sont levées que pendant l'exécution du programme. Pour
gagner du temps et écrire du code le plus fiable possible, il existe
cependant des outils qui permettent de trouver certaines erreurs avant
d'exécuter, et éventuellement en cours de frappe.

### Utilisation de Flake8

Nous allons commencer avec l'outil `flake8`, qui trouve à la fois des
problèmes de formatage de code et des bugs potentiels comme des
utilisations de variables ou fonctions indéfinies.

Modifiez le fichier `hello.py` en ajoutant une boucle "for" de la
manière suivante :

```python
#! /usr/bin/env python3

def dire_bonjour(nom):
    for i in range(3):
        print("Bonjour,", nom, "!")
        print("Et bienvenue à l'Ensimag !")

dire_bonjour("vous")
```

Analysons ce code avec `flake8` en lançant depuis un terminal la
commande suivante :

```
alphando@ensipc140:stage-unix$ flake8 hello.py
hello.py:7:1: E305 expected 2 blank lines after class or function definition, found 1
```

La commande nous indique qu'il devrait y avoir 2 lignes vides après la
définition de la fonction. Nous modifions notre programme en conséquence
et `flake8 hello.py` ne doit maintenant produire aucune sortie. Il n'y
a plus aucun problème avec notre programme selon les critères de
`flake8`.

Modifiez maintenant le programme en remplaçant la définition de la
fonction par `def dire_bonjour(arg)` (remplacer « nom » par « arg ») et
recommencez. Vous devriez obtenir l'avertissement suivant :

`hello.py:4:23: F821 undefined name 'nom'`

En effet, nous utilisons la variable `nom` qui est maintenant indéfinie
: c'est un bug. Le « F821 » n'est pas important : c'est le numéro de
l'erreur.

Revenez à la version précédente sans erreur.

### Utilisation de Pylint

Flake8 est content de notre petit programme, mais nous sommes plus
exigeants que cela. Nous allons maintenant dire à VS Code d'utiliser
l'outil Pylint, bien plus pointilleux que flake8.

Avant, on peut le vérifier en lançant la commande suivante depuis un
terminal :

```
alphando@ensipc140:stage-unix$ pylint hello.py 
************* Module hello
hello.py:1:0: C0114: Missing module docstring (missing-module-docstring)
hello.py:3:0: C0116: Missing function or method docstring (missing-function-docstring)
hello.py:4:8: W0612: Unused variable 'i' (unused-variable)

------------------------------------------------------------------
Your code has been rated at 4.00/10 (previous run: 4.00/10, +0.00)
```

`pylint` nous indique :

-   C0114 : qu'il manque un commentaire en haut du programme (juste
    sous le shebang) pour décrire ce que fait le programme
-   C0116 : qu'il manque un commentaire avant la fonction pour décrire
    ce que fait la fonction
-   W0612 : que la variable "i" n'est pas utilisée

Comment obtenir ce résultat directement dans VS Code ?

Pour ce faire, ouvrez le répertoire `stage-unix` avec VS Code :
`code stage-unix`. VS Code n'indique pour le moment aucun souci avec le
programme ni dans le résumé des erreurs/warnings en bas à gauche ni dans
le panneau "PROBLEMS".

![](img/vscode/VSCode-pylint-nothing_copie.png "VSCode-pylint-nothing_copie.png")

Pour activer 'pylint' dans VS Code, il suffit de d'installer l'extension `pylint` dans VS Code en suivant les instructions dans l'image suivante. L'affichage des messages est automatiquement activé par défaut avec cette extension. Pour la désactiver dans le projet en cours, il suffit de faire un clic-droit sur l'extension et de choisir l'option `Disable (Workspace)`.

![](img/vscode/VSCode-pylint-install.png ""){ width=90% }

<!-- 

Activons maintenant pylint via la *Command Palette* (`Ctrl+Shift+P`) les
commandes suivantes :

-   `Python: Enable Linting` et sélectionner "enable"
-   `Python: Select Linter` et sélectionner "pylint"
-   sauvegarder le fichier hello.py

Dans le panneau "PROBLEMS", l'erreur "Unused variable 'i'"
apparaît maintenant mais les warnings concernant les commentaires ne
sont pas affichés. Cela est normal car la [configuration par défaut de
pylint](https://code.visualstudio.com/docs/python/linting#_default-pylint-rules)
dans VS Code désactive un certain nombre de messages considérés comme
non vital pour la grande majorité des développeurs Python. Afin de
réactiver tous les messages, il faut modifier le fichier
`stage-unix/.vscode/settings.json`. Ce fichier permet d'outrepasser la
configuration de base de VS Code en la personnalisant pour tous les
fichiers dans le répertoire `stage-unix`. Vous pouvez ouvrir ce fichier
"settings.json" directement dans le navigateur de fichier à gauche en
dépliant le répertoire `stage-unix/.vscode`. Il ne reste plus qu'à lui
ajouter l'option `"python.linting.pylintUseMinimalCheckers": false` qui
permet de désactiver le comportement par défaut de pylint.

Votre fichier `settings.json` ressemble alors à :

```json
{
    "python.linting.enabled": true,
    "python.linting.pylintEnabled": true,
    "python.linting.pylintUseMinimalCheckers": false
}
```

Ce fichier a initialement été créé lors des actions précédentes dans la
*Command Palette*. La 1ère ligne `"python.linting.enabled": true`
correspond à l'activation du linting via `Python: Enable Linting` La
2ème ligne `"python.linting.pylintEnabled": true` correspond à la
sélection de pylint via `Python: Select Linter`

Une fois modifié, vous devriez avoir la joie de voir tous les messages
de pylint apparaître.

![](img/vscode/VSCode-pylint-enable_copie.png "VSCode-pylint-enable_copie.png") -->

Référence : <https://code.visualstudio.com/docs/python/linting>

### Lister les erreurs, naviguer entre les erreurs

L'affichage des erreurs (via des vaguelettes de couleur sous le terme
posant problème) dans le texte est pratique pour les éviter/corriger en
cours de frappe, mais sous-optimal quand il s'agit de corriger
plusieurs erreurs dans un fichier assez long (ce qui arrive souvent dans
la vraie vie, mais aussi dans certaines étapes du jeu de piste \...).
Heureusement pour nous le panneau PROBLEMS présente un résumé de
l'ensemble des `errors` et `warnings` suivi de leur position
(cliquable) dans un encadré en bas de la fenêtre de VS Code.

Comme dans la figure suivante, cliquer sur l'erreur "unused variable
'i'" vous amènera directement à la ligne concernée. On remarquera que
les erreurs sont soulignées par des vaguelettes rouges et les
informations par vaguelettes violettes

![](img/vscode/VSCode-pylint-lister-erreur.png "VSCode-pylint-lister-erreur.png")

Quelques généralités sur VS Code
--------------------------------

### Rechercher une chaine

Pour rechercher une chaine de caractères sous VS Code, il suffit
d'appuyer sur `Ctrl + F`, ce qui fait apparaitre une fenêtre en haut du
fichier édité. Nous allons analyser plus en détail cette fenêtre.

Le champ de saisie du haut permet de spécifier la chaine à rechercher.
Vous remarquerez qu'à partir de trois lettres frappées, VS Code
commence à effectuer une recherche lors de la frappe en indiquant le
nombre d'occurrences de la chaine dans le texte et en encadrant les
occurrences dans celui-ci. Vous pouvez passer d'une occurrence à une
autre en cliquant sur find ou en appuyant sur `Entrée` (la recherche
inverse se fait en appuyant sur `Shift + Entrée`).

VS Code vous propose par ailleurs d'effectuer un *chercher-remplacer*
en dépliant l'option de recherche. En effet si en plus de remplir le
champ du haut vous remplissez le champ du bas vous pouvez alors modifier
les chaines de caractères trouvées dans le texte par celle spécifiée
dans le champ du bas. Il vous suffit de cliquer sur `Entrée` pour
changer l'occurence en cours, ou bien sur *Replace All* pour changer
toutes les occurrences présentes dans le texte.

Par défaut VS Code effectue une recherche non sensible à la casse
(Majuscule/Minuscule sont traitée indifféremment) et se contente de
chercher uniquement la chaine donnée dans le champ du haut. Des options
pour la recherche sont aussi présentes sous la forme de trois boutons
situés à droite du champ de recherche afin d'effectuer des recherches
plus complexes . De gauche à droite ces options sont :

-   Match Case : Permet de devenir sensible à la casse (Majuscule et
    Minuscule sont traitées de manière indépendantes). Si vous cherchez
    la chaine *chaine* vous trouverez les occurrences de *chaine* mais
    pas celles de *Chaine*.
-   Match Whole Word : Traite la chaine à chercher comme un mot entier.
    Si vous cherchez *chaine* vous ne trouverez pas *chaines*.
-   Use Regular Expression : Cette option, si elle est active, permet de
    donner des expressions régulières dans le champ de recherche afin
    de, non pas matcher une chaine, mais un pattern. Si ce n'est pas
    déjà fait, vous apprendrez bientôt les détails de ce qu'est une
    expression régulière en cours de théorie des langages, et dans la
    partie avancée du cours d'Unix. Vous pouvez par exemple chercher
    tous les mots commençant par *cha* en utilisant la Regex
    "cha\[a-z\]\*" [Voici un petit mémo sur les
    Regex](http://www.expreg.com/memo.php).

![](img/vscode/VSCode-python-Control-F_copie.png "VSCode-python-Control-F_copie.png")

Notez que si vous sélectionnez du texte avant d'appuyer sur `Ctrl + F`
ce texte pré-remplira la zone de texte à chercher.

Vous pouvez également utiliser la vue Search en cliquant sur la loupe
dans barre d'activité. Le champ
"replace" est directement disponible. Attention toutefois car le
remplacement s'effectue sur tous les fichiers ouverts. Vous pouvez
appliquer le remplacement uniquement sur un fichier ou retirer les
fichiers non concernés dans la liste des fichiers contenant
l'expression à remplacer.

![](img/vscode/VSCode-python-Control-F-loupe_copie.png "VSCode-python-Control-F-loupe_copie.png")

Référence :
<https://code.visualstudio.com/docs/editor/codebasics#_find-and-replace>

### Exploration de la *Command Palette*

Lorsque vous entrez la combinaison `Ctrl + Shift + P` vous faites
apparaitre ce que VS Code appelle la *Command Palette*. Il s'agit
d'une sorte d'invite de commande de VS Code à partir de laquelle il
est possible d'exécuter l'ensemble des actions proposées par VS Code.

Ainsi vous pouvez effectuer un *Sauvegarder Sous\...* en frappant *Save
As*. Vous remarquerez d'ailleurs que la palette vous propose des
suggestions au cours de la frappe.

Vous pouvez aussi préciser un *scope* d'actions possibles. Par exemple
si vous voulez avoir l'ensemble des actions concernant la gestion de la
zone de texte, vous pouvez écrire *View:* dans la palette. Vous pouvez
par exemple divisez la zone en deux en sélectionnant *View:Split
Editor*.

Parmi les scopes les plus utiles on peut citer *View*, *Preferences* et
*Window* entre autres.

### .vscode et .config/Code/User

Nous allons maintenant aller un peu plus loin dans la configuration de
VS Code.

L'arborescence suivante vous donne un aperçu de quelques emplacements
ou fichiers clés pour VSCode

    ~ # racine de votre compte
    ├── .vscode
    │   ├── argv.json # argument de lancement de vscode
    │   └── extensions # extensions installées
    └──.config
        └── Code
            └── User
                ├── settings.json # réglages Utilisateur
                ├── keybindings.json # raccourcis Utilisateur
                ├── snippets 
                    └── python.json #les snippets pythons

Ces fichiers sont tous directement accessibles depuis VS Code depuis la
*Command palette* :

-   *Open Keyboard Shortcuts (JSON)* donne accès aux raccourcis par
    défaut ((`Preferences: Open Default Keyboard Shortcuts (JSON)`)) et
    raccourcis utilisateur
    (`Preferences: Open Keyboard Shortcuts (JSON)`)
-   *Settings (JSON)* donne accès à différents réglages (Default,
    Workspace, User, \...)
-   *Snippets* donne accès au fichier python.json permettant de
    configurer vos propres snippets
-   \...

Vous pouvez également accéder aux réglages directement dans VS Code via
la roue crantée
![](img/vscode/VSCode-icone-reglage.png "fig:VSCode-icone-reglage.png") et
`Settings` ou `Keyboard Shortcuts`

Concernant les réglages, ces derniers peuvent avoir différentes portées.
Ainsi il y a [plusieurs niveaux de
configurations](https://code.visualstudio.com/docs/getstarted/settings#_settings-precedence).
Les réglages d'un niveau s'ils existent sont prioritaires sur les
niveaux précédents.


|**Niveau de réglage** |      **''Command palette''**     |                                             **détails**                                            | 
|-----------------------------------------------------|-------------------------------|----------------------------------------------------------------------------------------------------------|
| par défaut (defaultSettings.json)                   | ''Default Settings (JSON)''   | Codé en dur. Uniquement accessible en lecture                                                            |                         |                 |             |
| Utilisateurs (.config/Code/User/Settings.json)      | ''Settings (JSON)''           | s'applique à toutes vos instances de VS Code                                                             |                         |                 |             |
| Workspace (.vscode/Settings.json)                   | ''Workspace Settings (JSON)'' | s'applique à l'espace de travail et à tous les répertoires. Remplace les réglages de niveau Utilisateur  |                         |                 |             |
| Workspace Folder (répertoire/.vscode/Settings.json) | ---                           | s'applique à tous les fichiers du répertoire. Remplace les réglages des niveaux Workspace et Utilisateur |                         |                 |             |


Référence : <https://code.visualstudio.com/docs/getstarted/settings>

### Interlude sur les *Snippets*

Des snippets sont des complétions automatiques que VS Code propose en
cours de frappe. Par exemple si dans votre fichier `hello.py` vous
commencer à taper `if` une boite de dialogue dans laquelle il y a deux
lignes sélectionnables :

-   -\>\| if `if condition: ...`
-   -\>\| ifelse `if condition:... else: ...`

Si vous cliquez sur l'une de ces lignes ou bien que vous la
sélectionnez à l'aide des flèches directionnelles et que vous appuyez
sur `TAB` VS Code génère automatiquement du texte correspondant au
squelette d'une condition `if` standard pour la première proposition,
et, pour la 2ème proposition,le squelette d'une condition `if` suivi de
`else`.

Malheureusement VS Code ne supporte plus nativement les snippets python
depuis [Janvier
2021](https://github.com/microsoft/vscode-python/blob/main/CHANGELOG.md#202110-21-january-2021).
Nous vous suggérons d'installer l'extension [python
snippets](https://marketplace.visualstudio.com/items?itemName=frhtylcn.pythonsnippets).

La figure suivante présente l'extension à installer et les snippets
associés au mot-clé `for=>`.

![](img/vscode/VSCode-python-snippets.png "VSCode-python-snippets.png")

Ainsi des squelettes de code pour les boucles ()`for`, `while`), les
instructions conditionnelles (`if`) sont à nouveau accessibles et bien
plus encore.

| Commande        | Effet                                               |
|-----------------|-----------------------------------------------------|
| `for=>`         | une boucle `for` qui parcourt une liste             |
| `range`         | `range(start, stop, step)`                          |
| `list.append=>` | un exemple d'utilisation d'append                   |
| `def=>`         | définition d'une fonction                           |
| ...             | tous les snippets sur le [dépôt github](https://github.com/ylcnfrht/vscode-python-snippet-pack#snippets--descriptons)                  |

Un peu d'aide pour le jeu de piste
-----------------------------------

Dans l'étape D3 du jeu de piste, on vous demande de faire
**rapidement** plusieurs corrections sur un fichier pour le compiler.
Voici quelques conseils pour aller vite.

### Décommenter une portion de code

Une fois sélectionnée, on peut décommenter une portion de code en
appuyant sur `Ctrl + Shift + :`. Remarquez que vous pouvez aussi
utiliser la *Command palette* et chercher *toggle line comment*. Cela
dit utiliser le raccourci clavier est beaucoup plus rapide. Si aucun
texte n'est sélectionné, VS Code commentera la ligne sur laquelle le
curseur est actuellement positionné.

Pour sélectionner tout le texte, on peut utiliser Menu
`Selection -> Select All`, mais le temps presse, on ira beaucoup plus
vite au clavier en appuyant sur `Ctrl + A`

En résumé : `"Ctrl + A, Ctrl + Shift + :"` (5 touches).

*N.B. Les commentaires ne se font pas de la même manière d'un langage
de programmation à l'autre. Pour que VS Code soit capable de
commenter/décommenter une portion de code dans le langage de votre
choix, il faut qu'il reconnaisse le langage actuellement utilisé.*
*Pour cela, VS Code se base sur les extensions de noms de fichiers. Par
exemple si vous ouvrez un fichier `hello.py` VS Code va considérer
qu'il s'agit d'un fichier `python` et va activer les fonctions
associées.* *VS Code affiche le langage actuellement interprété en bas à
droite de la fenêtre (`Python` pour un fichier `python`, `LaTeX` pour du
`LaTeX`, `Plain Text` quand il ne reconnait aucun langage, etc.).* *Si
vous cliquez sur cette information, vous pouvez forcer VS Code à changer
le langage actuellement reconnu.*

### Rechercher-remplacer

Pour remplacer toutes les instances d'une chaine par une autre chaine,
VS Code permet comme tout bon éditeur de texte de faire un
rechercher/remplacer. Comme nous l'avons vu précédemment il faut ouvrir
la boite de recherche depuis le menu `Edit -> Find` ou en appuyant sur
`Ctrl + F`. Vous entrez ensuite la chaine à chercher dans le premier
champs, et la chaine de remplacement dans le second champ (qu'il faut
déplier). Vous pouvez passer du premier champs au second en appuyant sur
`TAB`. Il suffit ensuite de cliquer sur l'icône *Replace All* en face
du 2ème champ.

En résumé :
`Ctrl + F, chaine1, déplier le 2ème champ, TAB, chaine2, Replace All`
(une petite dizaine de secondes)

### Corriger les erreurs rapidement

Pour exécuter le programme dans un terminal, vous devriez avoir une
commande comme

`python3 text_editor.py`

dans l'historique de votre shell. Pour exécuter, il suffit donc de
faire « flèche haut » (remonter dans l'historique), « Entrée » (2
touches).

En cas d'erreur à l'exécution, on peut utiliser la commande `Ctrl + G`
pour se positionner sur une ligne en particulier (*beaucoup* plus rapide
que de chercher la ligne à tâtons à la souris). Par exemple, si Python
vous affiche :

    $ python3 toto.py
      File "toto.py", line 15
        if 0 = 0:
             ^
    SyntaxError: invalid syntax

Il suffira d'entrer `Ctrl + G, 15, Entrée` (5 touches, environ 3
secondes) pour vous placer ligne 15.

L'autre option, probablement la plus efficace, est bien sûr d'utiliser
le linter Pylint pour passer d'une erreur à l'autre (cf.
[pylint](#lister-les-erreurs-naviguer-entre-les-erreurs)).

### Copier-coller rapide

Une fois le programme compilé et exécuté, on peut faire un copier-coller
du résultat « à la Unix » : sélectionner le texte à la souris (c'est
tout !), puis clic du milieu pour coller.

### Si ça ne suffit pas

Si vous entrez la chaine « moretime » dans le champ qui attend la
réponse, la page vous laissera plus de temps (le temps supplémentaire
est mémorisé d'une fois sur l'autre). Si vous maitrisez bien les
raccourcis claviers, vous ne devriez pas en avoir besoin.

Cheat Sheet
-----------

Un éditeur utilisé efficacement est un éditeur pour lequel on connaît un
certain nombre de [raccourcis
utiles](https://code.visualstudio.com/docs/getstarted/tips-and-tricks#_keyboard-reference-sheets).

Vous pouvez également modifier ou créer vos propres raccourcis via le
menu `File > Preferences > Keyboard Shortcuts`. Si vous avez envie de
ré-utiliser les raccourcis de votre éditeur de code préféré (Vim, Atom,
\...), vous pouvez les importer via `File > Preferences > Keymaps`

Référence : <https://code.visualstudio.com/docs/getstarted/keybindings>.

Fonctionnalités avancées
------------------------

### Workspace Trust

Pour paramétrer, utilisez `Preferences > Manage Workspace trust`

-   Workspace trust : Restricted mode
    -   Section "ouvrir un fichier"
    -   ref à une section "restricted mode"
        -   ne plus faire confiance au parent folder alphando
        -   revenir à une restricted
        -   \... en passant par paramètre
        -   <https://code.visualstudio.com/docs/editor/workspace-trust>

Références
----------

-   <https://en.wikipedia.org/wiki/Visual_Studio_Code>
-   <https://code.visualstudio.com/>
