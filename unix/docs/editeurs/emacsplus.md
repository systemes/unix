# Emacs

[**Emacs**](https://www.gnu.org/software/emacs/) est un éditeur de
tout texte (et donc de code) assez simple et très puissant, qui vous
permettra d'écrire vos programmes dans différents langages de
programmation (en particulier ceux enseignés à l'Ensimag, C, C++, R,
Python, Java, Rust, etc.), ainsi que vos rapports en LaTeX.

Pour vous faire une première idée, vous pouvez parcourir l'introduction
et la section finale, *more useful features*, de l'[« Emacs tour »
officiel](https://www.gnu.org/software/emacs/tour/) (en anglais).

Pour voir quelques capacités d'Emacs en action, vous pouvez regarder
les courtes capsules vidéos d'[Emacsrocks](https://emacsrocks.com).

## Débuter avec Emacs

Vous n'avez pas besoin de maîtriser complètement Emacs pour en tirer
profit. Vous pouvez l'utiliser en utilisant juste les menus et les
boutons.

Nos chers enseignants nous ont concocté [un petit
tutoriel](emacs.md) pour débuter sous
Emacs en éditant ses premiers programmes python.

Une fois cette première étape franchie, il vous faut décider si vous
souhaitez vous investir dans Emacs ou rester un simple utilisateur sans
grande ambition, et pour vous aider dans cette difficile décision...

## Pourquoi Emacs ?

Pour ne pas plagier la page Vim, voici un petit paragraphe qui va
essayer de vous convaincre d'utiliser Emacs. Il y a tout d'abord
l'argument traditionnel de la productivité qui veut que l'on programme
plus vite en utilisant Emacs (ou de même pour Vim), une fois bien
rôdé.  Ce n'est pas faux... mais est-ce que l'écriture de code est une
course de vitesse ?

Un autre argument est que vous ne deviendrez pas un meilleur
informaticien, ou mathématicien appliqué, en passant votre temps à faire
la partie monotone et répétitive du travail. Il vaut mieux laisser un
programme la faire pour vous et vous concentrer sur la partie créative.

Alors pourquoi utiliser Emacs ? Peut-être bien pour le confort qu'il
offre. Même si vous ne devenez pas un *die-hard Emacs fan*, vous
pouvez tout de même entrevoir les bénéfices qu'apporte Emacs en tant
qu'éditeur sur un logiciel plus simpliste comme nano, tels qu'une
véritable indentation automatique fondées sur la sémantique des
langages.

De plus, la flexibilité de l'éditeur vous permettra par la suite, si
vous le désirez, d'ajuster votre environnement de travail à vos
besoins. Une des grandes forces d'Emacs et peut-être la première est
en effet son extensibilité. Investir du temps dans Emacs vous
permettra, sans trop de peine, d'obtenir en retour un instrument sur
mesure. Vous pouvez ajouter tout ce que vous voulez avoir, et aussi,
plus exclusif, enlever ou remplacer tout ce qui ne vous plait pas.

Enfin, Emacs, comme vi/vim/neovim, ne va pas disparaitre
demain. Ils sont en développements actifs et ont des communautés
vivantes depuis plusieurs décénnies. L'investissement fait
aujourd'hui ne sera pas à jeter à la poubelle demain.

## Emacs chez vous

Emacs est disponible sur les machines de l'Ensimag, mais vous voudrez
sûrement l'avoir sur votre machine personnelle. Fort heureusement,
Emacs est disponible pour à peu près toutes les plateformes possibles et
imaginables et cela inclut Linux et tous ses petits
camarades unixoïdes, Mac OS X et
Windows. À chaque système son Emacs :

-   Si vous êtes sous un GNU Linux/Unix traditionnel, votre distribution
    possède un paquetage Emacs, voire plusieurs
    (faites une recherche) ; pensez à installer celui qui comporte une
    interface graphique, si la distinction est faite (regardez la
    description des paquetages). Certaines distributions proposent le
    choix entre plusieurs versions stable. La dernière est la version 28.x
    .
    -   Sous Ubuntu ou Debian, `emacs` pour la version graphique, et `emacs-nox`
        pour la version de terminal (détails
        [ici](http://doc.ubuntu-fr.org/emacs)).
    -   Sous Gentoo, le paquetage s'appelle app-editors/emacs.
    -   Vous pouvez également décider de compiler emacs depuis les
        sources, soit à partir d'une tarball, soit à partir du CVS de
        développement.

-   Si vous êtes sous Mac OS X, vous le trouverez dans votre
    gestionnaire de paquet [HomeBrew](https://brew.sh/) ou
    [MacPorts](https://www.macports.org/), ou la version [Carbon
    Emacs](https://emacsformacosx.com/).

-   Si vous avez Windows, le port officiel d'Emacs sous Windows
    s'appelle
    [NTEmacs](http://www.gnu.org/software/emacs/windows/ntemacs.html). Le
    téléchargement peut se faire [depuis le site officiel
    d'Emacs](https://www.gnu.org/software/emacs/download.html#nonfree),
    ou directement pour les utilisateurs de
    [Chocolatey](https://chocolatey.org/) ou
    [MSYS2](https://www.msys2.org)

## Utiliser Emacs

Emacs est programmable. Vous pouvez le modeler suivant vos besoins. Les
raccourcis-clavier sont les raccourcis par défaut. Vous pouvez les
changer, ou créer de nouvelles fonctions.

-   [Raccourcis claviers Emacs](Raccourcis_claviers_Emacs "wikilink")
-   [.emacs](Dot_Emacs "wikilink") : configurer Emacs.

## Ce qu'Emacs peut faire pour vous

Comme certains d'entre vous ont sûrement remarqué en fouillant dans les
menus, il y a un tas de choses dans Emacs, plus ou moins utiles. Pour
vous permettre de vous y retrouver, voici quelques petits pointeurs sur
des choses qui pourraient vous servir, à l'Ensimag :

### Pour coder

-   l'indentation automatique (suffit de presser TAB) ;
-   la compréhension du langage (ajouter `M-x eglot`) pour la syntaxe, et plus
-   la coloration syntaxique (ajouter `(global-font-lock-mode t)` dans
    le .emacs si nécéssaire) ;
-   la complétion automatique (avec M-/) ;
-   la compilation dans Emacs (M-x compile pour invoquer make(1), C-c
    C-m et C-c C-c pour compiler l'Ada avec gnatmake(1), C-c C-c pour
    le LaTeX) ;
-   le débogage dans Emacs (M-x gdb).

### Divers

-   l'explorateur de fichiers d'Emacs, dired, à mi-chemin entre un
    explorateur graphique et un shell (C-x d) ;
-   le lecteur de pages man (M-x man) ;
-   le navigateur de fichiers info (C-h i) ;
-   l'accès distant (SSH ou FTP) à vos fichiers (préfixer le chemin du
    fichier sur la machine distante par
    `/ssh:votrelogin@laditemachine:`, p.ex.
    `/ssh:bob@telesun:~/.emacs`), pour votre confort, à utiliser de
    préférence avec ssh-agent(1) et une [clef
    publique](Clés_SSH "wikilink") ;
-   la calculatrice scientifique programmable, intégrales et dérivées symboliques inclus (M-c calc)
-   l'édition collaborative (package crdt)

Certaines des commandes données ci-dessus (gdb et dired, entre autres)
vous transporteront dans un mode différent (dans un nouveau buffer), qui
vous sera alors inconnu ; pour obtenir des informations de base sur
l'utilisation de ce nouveau mode, n'oubliez pas la commande C-h m.

## Trouver plus de documentation

### Pour débutants
-   Tout est documenté dans Emacs (Menu Help)

-   Le tutorial intégré à Emacs : dans le menu `Help` d'Emacs.

-   La [page du Wiki Emacs pour débutants et en
    français](http://www.emacswiki.org/cgi-bin/emacs-fr?NouvelUtilisateurEmacs)
    !

-   L'[« Emacs tour »
    officiel](http://www.gnu.org/software/emacs/tour/) (en anglais)

-   Une [introduction simple a
    Emacs](http://p.karatchentzeff.free.fr/freesoft/emacs/html/index.html)
    : utile surtout si vous êtes encore vraiment un débutant.

-   [À la découverte de GNU
    Emacs](http://people.via.ecp.fr/~flo/2000/emacs-tut/) : adressé
    également aux débutants, mais aussi aux débutants en Lisp, donc, pas
    si débutants que ça.

### Pour tous

-   [Emacs Wiki](http://www.emacswiki.org/) : le Wiki de référence sur
    Emacs.

-   [Le manuel
    d'Emacs](http://www.gnu.org/software/emacs/manual/html_node/emacs/index.html)
    : très complet.

-   [Le manual d'Elisp (Emacs
    Lisp)](http://www.gnu.org/software/emacs/manual/html_node/elisp/index.html)
    : utile si vous souhaitez vous lancer sérieusement dans la
    personnalisation de votre Emacs, et comme référence pour certains
    points techniques et fonctionnalités avancées (advices, macros...).

-   [1](http://emacsrocks.com) Des vidéos thématiques du compte twitter
    @emacsrocks

-   [2](http://sachachua.com/blog/emacs/) Le blog de Sacha Chua, qui
    suit l'actualité de emacs

-   [Awesome Emacs](https://github.com/emacs-tw/awesome-emacs) avec le
    commentaires et l'explication des «modes» les plus populaires.

-   Le newsgroup
    [fr.comp.applications.emacs](http://groups.google.com/group/fr.comp.applications.emacs/)
    : un très bon endroit pour poser des questions, les archives sont
    une mine d'or d'informations.

