# Objectifs du stage UNIX de rentrée

L'objectif du stage UNIX est de vous faire
**comprendre les concepts des systèmes d'exploitation en les manipulant**
(processus, fichiers, mémoire, parallélisme, droits, interface de
manipulation de texte, etc.). Ils sont sous-jacents dans tous les
cours/TD/tp de l'école. Plus tard, dans votre vie active, ils seront
également là.

À court terme, notre but est vous rendre **autonomes** et **efficaces**
sur votre poste de travail GNU/Linux. Avec juste un peu pratique, vous
constaterez que vous deviendrez capables de faire **mieux** et **plus vite**.

À long terme, notre but est de vous permettre de vous forger votre
propre **opinion** sur ce qui a du **sens** et ce qui n'en a pas, ce qui
marche et ce qui ne marche pas, et surtout de rester **libre** de vos
choix, en vous apprenant à regarder les détails avec **rigueur** pour
éviter de vous mettre des barrières dénuées de sens.

Et tout cela en seulement 7h30 de TP 😊

* [Programme et attendus](#programme)
* [Débutantes et débutants en UNIX: Poly et Jeu de Piste](#debutants)
* [Expérimentées et expérimentés  (Prépas MPI, CPP majeur Info, L3 Info, BUT 3 Info, volontaires): Atelier](#trackZ)
* [La Linux Install Party (LIP)](#lip)


## En pratique, pendant le stage de septembre... {#programme}

Nous proposons deux parcours: **Débutants et Débutantes**, et
**Expérimentées et Expérimentés pour les Prépas MPI, CPP majeur Info,
L3 Info, BUT 3 Info, et les volontaires**, bref ceux ayant déjà les
bases en Unix.

C'est le niveau **Débutants et Débutantes** qui est évalué à
l'examen. Les supports de ce parcours n'ont pas de pré-requis, sont
progressifs et très guidés.

L'atelier **Expérimentées et Expérimentés** concerne, comme l'autre,
les processus, les fichiers, et la mémoire. Mais il demande de savoir
déjà se servir d'un système GNU Linux (processus, fichiers, mémoire,
terminal, commandes de base, etc.).  Le contenu des sujets de TP du
parcours **Expérimentées et expérimentés** est 
**volontairement plus stimulant** (plus ardu et moins guidé),
calibré pour occuper les étudiants et les étudiantes fortes en Unix
pendant 1h30. Le but de faire le travail en groupe est que les plus
rapides entrainent les autres pour que tout le groupe puisse terminer
le sujet.

L'atelier **Expérimentées et Expérimentés** commence en faisant le
parcours débutant, mais sur une seule séance, pour que tout le monde
ait vu ce qui sera évalué à l'examen.

### Le programme du stage de rentrée 📅 

| Séances (1h30) | Débutantes et débutants | Expérimentés et expérimentés   |
|----------------|-------------------------|--------------------------------|
| 1              | Guide <=> Jeu de Piste  | Speed run Guide/Jeu de piste   |
| 2              | Guide <=> Jeu de Piste  | Atelier 1: HOME                |
| 3              | Éditeurs universels     | Éditeurs universels            |
| 4              | Guide <=> Jeu de Piste  | Atelier 2: Machines virtuelles |
| 5              | Guide <=> Jeu de Piste  | Atelier 3: Conteneurs          |



## Débutants et débutantes {#debutants}
### Les deux ressources pédagogiques des 5 séances de début septembre

- Le **polycopié d'UNIX** : à lire, linéairement pour les débutants. Il contient aussi quelques pépites pour épater les experts pendant leur speed-run : le guide _« Initiation à Unix; L'environnement de travail à l'Ensimag »_ disponible en version [HTML](http://systemes.pages.ensimag.fr/www-unix/html/index.html) ou en [PDF](http://systemes.pages.ensimag.fr/www-unix/poly-intro-unix.pdf). Son contenu est progressif, avec 80 manipulations, et couvre toutes les notions importantes !
- Le **jeu de piste (Sur les machines de l'Ensimag)** : une promenade ludique dans l'univers d'UNIX. La page décrivant le jeu de piste est ici : [jeu de piste](jeu2piste.md) ;
### Autres activités
- La documentation pour **l'installation de Linux de votre machine personnelle**. Vous verrez, l'eau est bonne et pas aussi profonde que vous le croyez ! La page décrivant l'install party est ici : [Linux Install Party](http://systemes.pages.ensimag.fr/www-unix/lip/html/lip.html).
- Un **examen de TP** qui se déroulera en salles PC ([Version de démonstration](http://lig-enseignement.imag.fr/jeu-de-piste/demo-exam-ensimag-fr/) de l'examen, pour s'entraîner. Bien sûr, le vrai examen sera plus long !)

??? note "Consignes (à déplier) concernant l'examen de TP"
    - Le seul document papier autorisé est une **feuille A4 manuscrite recto-verso**, 
    où vous pouvez écrire **à la main** tout ce qui vous passe par la tête (et préférentiellement ce qui pourrait vous être utile pour l'examen…)
    - Le poly vous sera fourni au format PDF et au format HTML pendant l'examen.
    - L'examen de TP est long. Il contient plus d'une vingtaine de questions, et il faut environ une demi-heure à un utilisateur d'Unix rapide et expérimenté pour le terminer. Donc :
        - Préparez-vous à être rapides. Refaire le jeu de piste en entier avant l'examen est sans doute une bonne idée.
        - Relire le poly.
        - Pendant l'examen, ne vous attardez pas sur les questions que vous trouvez trop difficiles. Les questions étant indépendantes, il est facile de laisser une question de côté...
    - Pour les examens de TPs, vous travaillerez sur un compte initialisé pour l'occasion, sans configuration particulière. Vous n'aurez pas accès à vos comptes, aux serveurs habituels (pcserveur, ...) ni à Internet.
    - Certaines étapes mettent volontairement du temps à donner la réponse. Ces questions apparaitront en début de liste de questions, il est conseillé de les démarrer en premier (pour pouvoir avancer sur les autres questions en attendant la réponse).

### Déroulé du stage et progression conseillée

Voici une proposition de progression, suivant votre niveau d'expertise de départ. L'important dans ce stage est de découvrir jusqu'où vous avez tout compris et de faire quelques pas de plus pendant les séances.

!!! help "Minimum vital, niveau débutant"
    1. Lire et faire les différents exercices du poly ([HTML](http://systemes.pages.ensimag.fr/www-unix/html/index.html) ou [PDF](http://systemes.pages.ensimag.fr/www-unix/poly-intro-unix.pdf)) ;
	
       | Numéro de séance (1h30)     | 1 | 2 | 3        | 4 | 5  |
       |-----------------------------|---|---|----------|---|----|
       | Chapitre du poly à terminer | 2 | 5 | Éditeurs | 7 | 10 |
	   
    2. En parallèle de cette lecture, suivez-le [jeu de piste](jeu2piste.md) ;
    
	Si vous terminez plus vite, nous vous conseillons de passer du temps à apprendre à vous servir de Linux, après l'avoir installé sur votre machine perso (installation de logiciels, configuration, notamment du VPN) ; Faire le mini-tutoriel [Premiers pas avec Visual Studio Code](editeurs/vscode.md) ; Lire la page [Trucs et astuces Unix](tips/tips.md) (il y a quelques redites avec le guide, mais ça serait dommage de passer à côté !) ; Parcourir les pages des [outils numériques à l'Ensimag](services/ensimag.md).

!!! warning "Niveau Intermédiaire"
    1. Faire les deux jeux de piste pour vérifier que vous avez
       atteint le niveau attendu
	2. Configurer votre Thunderbird (cf. chapitre 3 du guide pour les
       informations)
    3. Apprendre par cœur la commande qui compte le nombre de vos fichiers à partir du répertoire courant **INDISPENSABLE SI VOUS UTILISEZ VSCODE**
	   ```console
	   du -hs --inodes * .[^.]* ..?* | sort -h
	   ```
    4. Parcourir le [wiki des BuBus](https://bugbusters.pages.ensimag.fr/wiki/) ;
    5. (Séance 3) S'initier à l'éditeur [Vim](editeurs/vim.md) en lançant `vimtutor` ;
    6. (Séance 3) S'initier à l'éditeur [Emacs](editeurs/emacsplus.md) en lançant son tutoriel dans son menu _Help_ ;
    7. S'inscrire dans [TEIDE](https://teide.ensimag.fr/), le service de rendu de TP de l'Ensimag, et y rendre un fichier quelconque (cette fois-ci, c'est juste pour s'entraîner, les prochaines fois seront notées !).

!!! failure "Experte ou expert qui s'ignore et qui aurait dû faire les ateliers"
    1. [Utiliser un laptop à deux simultanément](http://systemes.pages.ensimag.fr/www-unix/defis/partage-d-un-laptop.pdf)
    2. [Comprendre le modèle de donnée de Git](http://recherche.noiraudes.net/resources/git/TP/tp1-modele-git.pdf)
    3. [Gérer l'historique des modifications dans Git](http://recherche.noiraudes.net/resources/git/TP/tp2-historique.pdf)

# Expérimentées et expérimentés pour prépas MPI, CPP majeur Info, L3 Info, BUT 3 Info, et volontaires) {#trackZ}

Pour ces ateliers, nous demandons à l'ensemble des étudiants
expérimentés de se grouper physiquement dans la salle pour pouvoir discuter ensemble
lors de votre progression à partir de la séance 2. Dans la population à l'entrée de l'Ensimag,
ce groupe devrait contenir (ordre par quantité) les Prépas MPI, les CPP majeur Info (Prépas intégrées des Ensi), L3 Info, et les BUT 3
Info. Si vous savez utiliser GNU Linux couramment (terminal, commandes de bases),
n'hésitez pas à vous joindre à ce groupe.

### Speed run sur le guide et le jeu de piste

Votre première séance consite à parcourir les deux contenus pédagogiques des
débutants. Vous y apprendrez des choses et aurez une idée du niveau de
connaissance attendu à l'examen.


### Atelier HOME
Le sujet concerne votre [HOME, la compilation d'un exécutable et son
installation dans votre HOME, et une configuration multi-logiciels
complexe](https://systemes.pages.ensimag.fr/www-unix/atelier/home-compile-config.pdf)
### Atelier Machine virtuelle et SSH
Le sujet traite de [la mise en place d'une machine virtuelle pour y faire tourner un logiciel complexe (Jami)](https://systemes.pages.ensimag.fr/www-unix/atelier/ssh-vm.pdf)

### Atelier Conteneurs
Le sujet vous fait faire [une image de conteneur permettant d'exécuter trois
logiciels rigolos d'un environnement GNU Linux/Unix](https://systemes.pages.ensimag.fr/www-unix/atelier/conteneurs.pdf)


## Éditeurs universels (Séance 3, pour tout le monde)

 Le but de cette séance **les deux groupes ensemble** est d'acquérir la
compréhension commune de ce que regroupe l'activité d'édition de
texte, centrale dans votre future vie scientifique.

_VS Code_ n'exécute pas du python et il ne comprend pas le C. Il ne
compile pas le Latex. Il ne gère pas vos sources git. Il n'a pas de
*shell*. Il ne déplace pas des fichiers dans des
répertoires. Pourtant, il vous donne l'impression de savoir faire tout
cela.

C'est en masquant l'existence des outils externes utilisés que cet éditeur
est perçu, comme le plus facile, le plus rapide, le plus
séduisant (car, en plus, il est joli).

C'est pour soulever le masque que vous allez apprendre à utiliser les
deux éditeurs universels ultra-connus, Vim et Emacs, très répandus et
libres. Ils sont plus explicites sur ce qui est propre à l'éditeur et
ce qui est à l'extérieur, chacun à sa façon. **Pour garder la liberté
de choisir le bon outil au bon moment, il faut connaitre le nom des
outils dans la boîte à outil**.

Les racourcis d'Emacs et Vim/Néovim sont partout dans vos autres
logiciels, en commençant par votre shell. Ils ont le bon goût de
fonctionner aussi dans un terminal texte, ce qui vous servira lors du
travail à distance (avec Ssh). Ils ont une communauté forte qui les
développe depuis plusieurs décennies. Ils sont utilisés,
communautaires et libres.

??? note "VS Code est-il là pour toujours ?"
    Non, ou pire, d'ici à
    la fin de vos études (3-5 ans), il pourrait passer de mode. (Liste des précédents _à la mode_ à l'Ensimag depuis 2000: nedit, netbeans, gedit, eclipse, sublimetex, atom) . VS Code n'est
    pas entièrement libre et n'est pas développé par sa communauté. Il dépend de la volonté de Google (pour Electron, ce qui fait l'affichage) et de Microsoft
    (pour les 2 millions de lignes de Typescript de l'éditeur, hors
    affichage). Et l'arrêt peut être rapide. L'exemple récent est Atom,
    l'éditeur de Github, qui était celui à la mode, avant le rachat de
    Github par Microsoft. 

* **30 minutes**: Faire le début du **tutoriel de Vim** (en anglais) en lançant la commande `vimtutor` (ou si vous avez lancé `nvim`, taper `:Tutor`)
* **30 minutes**: Faire le début du **tutoriel d'Emacs** en lançant le tutoriel (en français, ou une autre langue) dans son menu Help.

## Préparation de la [Linux Install Party](http://systemes.pages.ensimag.fr/www-unix/lip/html/lip.html) {#lip}

- _**Sauvegardez vos données !**_ Indépendamment de la LIP, plusieurs
      disques et SSD de la promo vont physiquement tomber en panne
      dans l'année !
- _**Sauvegardez vos données !**_ Chaque fois que vous manipulez vos
  données, et ce sera le cas pendant la LIP, vous n'êtes pas à l'abri
  d'une fausse manœuvre, incompréhension, d'ignorer un avertissement,
  voir de cliquer un petit bouton dangereux de Windows, sans confirmation ni
  avertissement, ce qui lui donne l'air bien inoffensif (Bouton de
  conversion BIOS-GPT, je pense à toi), etc.
- Identifiez votre matériel et votre système
- Désactiver Bitlocker sous Windows. En 2022 et 2023, il est activé
  sur 15% des laptop de la promo. Le désactiver prend de 10 à 40
  minutes. Le risque majeur est de rester coincé sans pouvoir lire
  votre disque même pour des actions bénignes. Vous pourrez le
  remettre après la LIP.
- *Faire de la place pour Linux* (au moins 50-70 Gio) (Nous ferons la
  partition pendant la LIP, mais il faut de la place sur le disque)
- Apprendre à démarrer votre UEFI/BIOS
-  **Sauvegardez vos données !**_

## BONUS pour les curieux ayant du temps à gagner

### Les logiciels

Une des forces des distributions GNU/Linux est la grande facilité
d'installation et de maintenance des logiciels. Contrairement aux
''stores'' (Android, IOS, MS Store, etc.) dans lequel chaque logiciel
''vit'' isolé dans son silo, les logiciels libres de votre
distribution forment un tout et sont gérés globalement simultanément.

* Compiler et installer à la main chez vous sur votre compte Ensimag

Puis, dans une VM d'une Debian Live (il faut être administrateur)
* les paquets logiciels, avec dpkg
* les gestionnaires, avec apt
* les gestionnaires de binaires multi/distributions avec snap
* les entrepôts de logiciels avec guix

* [Le sujet sur la gestion des Logiciels](http://systemes.pages.ensimag.fr/www-unix/defis/logistique-logicielle.pdf)

### Programmation Bash

Le but est d'écrire un générateur de galeries d'image pour le web, en Bash.
Vous lancez le script en lui donnant un répertoire de départ contenant les images et un répertorie d'arrivée où le script créera les pages web en HTML, les vignettes des images et les images elles-mêmes. 

-   Programme shell générant une page HTML « galerie
    d'images »:
    [le sujet](http://systemes.pages.ensimag.fr/www-unix/defis/bash-gallerie-images.pdf)
-   Squelette BONUS pour démarrer le TP :
    [le code de départ](https://systemes.pages.ensimag.fr/www-unix/avance/tpl/squelette-tpl-unix.tar.gz)
-   Exemples de pages HTML et de galeries d'images :
    [./tpl/exemples-html/](https://systemes.pages.ensimag.fr/www-unix/avance/tpl/exemples-html/)
-   Quelques exemples de scripts shell qui peuvent vous aider :
    [./tpl/exemples-shell/](https://systemes.pages.ensimag.fr/www-unix/avance/tpl/exemples-shell/)
-   La page [Erreurs fréquentes en Bash (dans les scripts
    shell)](https://systemes.pages.ensimag.fr/www-unix/avance/Erreurs_fréquentes_dans_les_scripts_shell).

### SSH & Co

Le but de ce TP est ici de manipuler GNU/Linux à distance. Il permet
de travailler le terminal, la gestion des processus et des fichiers
(distants).

* [Travailler à distance (SSH, TMUX, X11)](http://systemes.pages.ensimag.fr/www-unix/defis/travail-a-distance.pdf)

### Processus (et fichiers) dans un Linux moderne

Dans un Linux moderne, il existe de nombreuses façons de lancer des processus autrement qu'en utilisant les capacités classiques du système (shell, icônes, menus). Depuis 20 ans, Linux est très en pointe sur ce domaine, ce qui explique en partie sa dominance dans le ''Cloud''. Dans ce TP, on utilisera

* Les machines virtuelles (VM), avec qemu. Cela permettra d'avoir un
  système dans lequel vous serez administrateur, pour les points suivants.
* Les conteneurs, avec docker
* Les prisons, avec firejail
* Les surcouches généralisées (''overlay'') avec guix

* [Le sujet du TP sur les processus dans un Linux moderne](http://systemes.pages.ensimag.fr/www-unix/defis/processus-modernes.pdf)
