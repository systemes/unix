Ce document présente quelques trucs qui rendent la vie plus agréable
sous Unix.

Dans un terminal
----------------

(ce qu'on appelle « terminal », c'est la fenêtre dans laquelle on
exécute des applications en mode texte, en particulier le shell, qu'on
appelle parfois aussi « ligne de commande »)

### Complétion automatique avec TAB

Mettons que vous vouliez exécuter le fichier script `hello.py`. Il faut
taper la ligne de commande

```
python3 hello.py
```

En fait, il suffit de taper `pyth`, puis d'appuyer sur TAB, le shell
complète en `python`, ajoutez 3 et l'espace. On tape maintenant le
début de `hello.py`, et TAB complète. Si il y a plusieurs solutions,
deux pressions sur TAB donnent la liste des possibilités, on tape ce
qu'il manque, et on refait TAB.

Ça permet d'aller *beaucoup* plus vite, et ça évite les fautes de
frappes. Si le shell a complété, c'est que le fichier existe !

### Le « ls » en couleur

Dans un terminal, essayez la commande

```
ls --color
```

Sympa ? Si vous voulez que `ls` soit tout le temps en couleur, ajoutez
la ligne suivante à votre fichier `.bashrc` :

```
alias ls='ls --color=auto'
```

### Ré-exécuter une commande (en la modifiant si besoin)

Dans un shell, après vous êtes déplacés dans un répertoire à l'aide de la commande `cd`, faites «
Ctrl-r », puis `cd` ». Le shell va rechercher dans l'historique une
commande contenant la chaîne `cd`. Si besoin, éditez cette ligne, par exemple en
vous déplaçant avec les flèches, et exécutez la commande avec la touche
Entrée.

Une autre solution est d'utiliser la commande `!`. Oui oui, vous avez
bien vu, il s'agit d'un simple point d'exclamation. Cette commande
très puissante permet de naviguer dans l'historique, de réutiliser
seulement un ou tous les arguments d'une commande antérieure. Par
exemple `!-1` exécute la commande précédente (équivalent à presser la
flèche « ↑ » ), tandis que `!-6` exécutera la 6-ième dernière
commande. Une autre application utile est de taper `history` puis `!n` avec `n` le numéro d'une ligne renvoyée par `history`. Mieux,
tapez `!abc` pour exécuter la dernière commande **commençant** par `abc`, par exemple `!ged` relancera ma commande `gedit toto.txt`. Il ne s'agit là que de l'utilisation la plus basique, pour
plus de détails, consultez `man history`
[1](https://man7.org/linux/man-pages/man3/history.3.html)

L'environnement graphique
--------------------------

### Déplacer une fenêtre avec Alt-souris

Pour déplacer une fenêtre rapidement, maintenir la touche Alt enfoncée,
et faites glisser la souris n'importe où sur la fenêtre à déplacer.

### Le copier-coller à la souris

La plupart des applications permettent le copier-coller avec les
raccourcis Ctrl-c, Ctrl-x, Ctrl-v (comme sous Windows), mais sous Unix,
on a une alternative très pratique, tout à la souris : On sélectionne du
texte avec le bouton gauche, et on colle avec le bouton du milieu.

Essayez dans un terminal. Par exemple, tapez `ls`, puis sélectionnez un
nom de fichier (par exemple, `hello.py`). Tapez `code`, puis appuyez sur
le bouton du milieu, avec le curseur dans la fenêtre. Le prompt est
maintenant `code hello.py`, il n'y a plus qu'à appuyer sur Entrée pour
valider.

### Changer de fenêtre avec Alt-tab

Comme sous Windows, on peut changer de fenêtre avec Alt-tab (plusieurs
fois jusqu'à tomber sur la bonne fenêtre).

### Traitement de texte, tableur et présentation

Si vous cherchez une suite de logiciels semblables à ceux de Microsoft
Office, vous avez la suite LibreOffice et son ancêtre [Open
Office](http://fr.openoffice.org/), qui sont installées à l'Ensimag et qui
sont librement disponibles et installables sur de multiples plateformes
dont Windows, Linux et MacOSX. Il existe d'autres façons de rédiger un
rapport (comme LaTeX) ou de faire une présentation (comme LaTeX avec le
style beamer).

Apprendre à taper vite
----------------------

Un bon informaticien doit savoir utiliser ses dix doigts sur un clavier,
et taper en regardant l'écran. La position au repos est : main gauche
sur « qsdf », main droite sur « jklm » (normalement, il y a des petits
ergots sur les touches correspondant aux index), et pouce droit sur la
barre d'espace. L'apprentissage est long, mais il en vaut la peine.

Il existe de nombreux logiciels permettant de s'entrainer à taper vite
et bien, par exemple

-   [Ktouch](http://ktouch.sourceforge.net/) : un logiciel libre disponible sous Linux
    et autres Unix.
-   [Des tas
    d'autres](http://www.01net.com/Total.php?searchstring=dactylo&system=windows&x=15&y=10)
    pour Windows (n'hésitez pas à préciser si vous en connaissez un
    mieux que les autres pour le recommander aux étudiants).
