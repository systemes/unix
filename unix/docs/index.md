# Ensimag 1A (niv. L3) - Unix: Bienvenue ! ✨

Bienvenue sur la page du cours d'Introduction à UNIX !  Vous trouverez
ici toutes les ressources nécessaires pour suivre ce cours, ainsi que
quelques documents annexes permettant d'approfondir certains concepts.

Cet enseignement est construit pour vous faire comprendre **les
concepts fondamentaux des systèmes d'exploitations en les
pratiquant** (processus, mémoire et fichiers). Il s'adapte à votre niveau de départ ("débutants" ou
bien "avancés") pour aller plus loin, plus profond, plus complet.

## Comment se déroulera votre apprentissage à l'Ensimag

Le but de tous vos enseignants est de transmettre, à chacun, des
connaissances, c'est-à-dire des savoirs (la théorie) que vous savez
appliquer dans différents contextes (la pratique).

Les enseignements universitaires sont construits comme vos 15 ans de
cours de mathématiques: chaque cours utilise toutes les connaissances
précédentes, supposées acquises.

**Jouez avec vos connaissances en les appliquant de manière variée, avec patience et rigueur**:
utilisez-les dès qu'une occasion se présente, posez-vous des questions
absurdes, appliquez les dans des contextes étranges, tester les
limites, etc. C'est notre meilleur conseil. Il est valable pour le
reste de votre carrière en informatique et mathématiques
appliquées.


Une autre copie de cette page est disponible
[ici](https://systemes.gricad-pages.univ-grenoble-alpes.fr/unix/).


## Déroulement des TP et calendrier 📅

Ce cours d'introduction à UNIX est composé de trois séquences temporelles :

1. le [stage UNIX de début septembre](stage), dont le but est
   d'introduire ou approfondir quelques concepts fondamentaux de
   l'informatique. Pour les débutants, il est constitué d'un
   polycopié électronique et d'un jeu de piste à réaliser de façon
   individuelle (une seule personne par machine). Pour les
   expérimentés, il est composé d'un atelier pratique en
   groupe (une seule personne par machine). Dans les deux cas, n'hésitez pas à poser des questions aux
   enseignants ou à répondre à celles de vos camarades !

2. La [_Linux Install Party_ (soirs de fin septembre)](http://systemes.pages.ensimag.fr/www-unix/lip/html/lip.html),
   pour vous aider à installer Linux sur votre laptop
   personnel. L'installation en elle-même vous impose de comprendre
   . Le but est de vous permettre une utilisation quotidienne facile
   de Linux sur votre machine.

3. la partie [UNIX avancé (début janvier)](http://systemes.pages.ensimag.fr/www-unix/avance), qui
   comme son nom l'indique, va plus loin en présentant de manière plus
   poussée certains outils ou notions, comme les scripts shell, `git`
   ou bien la notion d'environnement. Là encore, un certain nombre de
   sujets de TP vous sont proposés.

Côté calendrier, le stage UNIX se déroule dans la période dite du
_stage de rentrée_ (= 3 premières semaines de cours en septembre), sur
5 séances (+2 séances bonus pour l'install party). La partie UNIX
avancé a lieu au mois de janvier et s'étale sur 5 séances.

## Évaluation

L'évaluation de ce cours porte sur un examen individuel sur
machine. Cet examen vous demandera de reproduire un certain nombre de
manipulations déjà vues lors du stage UNIX (débutants), et de reporter
vos réponses sur une interface web dédiée.

## Enseignants du stage de septembre

-  Olivier Alphand
-  Danilo Carastan dos Santos
-  Grégory Mounié (responsable)
-  Lucie Muller
-  Pierre Neyron
-  Frédéric Pétrot
-  Pierre Ravenelle

## Le pourquoi de ces séances autour d'Unix/Linux pour tout le monde

Chaque enseignement universitaire s'appuie sur les connaissances
données lors des enseignements précédents. Ces connaissances sont
supposées acquises.

En deuxième année, dans les cursus de mathématiques appliquées, le TP
de modélisation mathématiques en IA, 1 h30 sur les neurones profonds,
vous demandera de manipuler uniquement dans un terminal des machines
distantes. Vous leur ferez télécharger directement de grands modèles dans
des répertoires ne vous appartenant pas, puis lancer les exécutions des calculs
en utilisant leurs GPGPU.

Personne ne vous expliquera, à ce moment-là, les commandes unix de
base à taper dans le terminal pour la gestion des processus, des fichiers et des répertoires
(vu dans ces TPs, 1 an avant), l'architecture des processeurs et le
logiciel de base (cours de 1A second semestre) et la
programmation des GPGPU (cours en 2A). mais ils seront pourtant tout
aussi nécessaires que l'algèbre pour faire ce TP en 1 h30.

Linux et les autres UNIX seront l'environnement de votre travail à
tous, pendant vos études, parce qu'il est le meilleur support
disponible pour notre pédagogie. Ils seront aussi votre environnement de
travail après votre diplôme pour la très grande majorité.

