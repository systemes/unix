# Le Jeu de Piste

Contenue de cette page:

* [Jeu de piste de base, en lien avec le guide](#jdpbase)
* [Jeu de piste niveau 2, mins guidé](#jdpavancé)

## Jeu de piste, niveau 1 (grand débutant, complémentaire du guide) {#jdpbase}

Pour rendre le stage plus ludique, vos enseignants vous ont préparé un TP d'auto-évaluation sous la forme d'un jeu de piste. Le principe est simple : chaque étape vous demande de trouver une manipulation dont l'exécution vous permet d'accéder à l'étape suivante.

La plupart des étapes sont faites pour être réalisées depuis votre machine à l'Ensimag. Si vous voulez travailler à distance, vous pouvez le faire en vous connectant par exemple à `pcserveur.ensimag.fr` (cf. [le site des BuBus](https://bugbusters.pages.ensimag.fr/wiki/ensimag/) pour savoir comment faire pour se connecter depuis chez soi à une machine de l'école).

Vous êtes encouragés à vous entre-aider, par contre, jouez le jeu : évitez de donner à vos collègues la solution d'une étape qu'ils n'ont pas encore atteinte.

La dernière étape est l'étape G est le tutoriel sur ssh.

### Si votre compte Ensimag fonctionne

Prêts ? C'est parti ! Voici la première étape : [étape A1 (VPN obligatoire si vous n'êtes pas sur une machine de l'Ensimag)](https://gitlab.ensimag.fr/systemes/3mmunix/-/blob/master/README.org).

### Si votre compte Ensimag ne fonctionne pas encore sur les serveurs, mais que vous êtes connecté sur une machine de l'Ensimag

L'étape suivante est une exécution de programme Python (si vous
préférez le C ou l'Ada, voir ci-dessous). Le programme se trouve dans
le fichier etape_b1.py dans le répertoire jeu-de-piste sur le compte
de l'utilisateur _jdpunix_.

Vous n'avez pas le droit d'utiliser la commande 'ls' dans ce
répertoire (vous pouvez essayer, mais ça ne marchera pas), mais vous
pouvez tout de même récupérer le fichier en question (vous verrez plus
tard comment utiliser la commande chmod pour obtenir ce genre de
permissions).

Récupérez ce fichier chez vous, par exemple avec

```bash
cp le-fichier-en-question ~
```

(~ veut dire 'mon répertoire personnel')

Puis revenez dans votre répertoire personnel et exécutez ce fichier
avec la commande

```bash
python3 etape_b1.py
```

Le programme vous donnera les indications pour aller a l'étape
suivante.

Si vous préférez le langage Ada, un programme Ada se trouve dans le
fichier etape_b1.adb dans le répertoire jeu-de-piste sur le compte de
l'utilisateur _jdpunix_. Copiez le fichier sur votre compte, puis
compilez-le avec la commande :

```bash
  gnatmake etape_b1
```

puis exécutez-le avec

```bash
./etape_b1
```

Si vous préférez le langage C, une version C se trouve dans le même
répertoire, dans le fichier etape_b1.c (à compiler avec la commande
gcc).


-------------------------------------------------------------------------------

## Jeu de piste, niveau 2 (moins guidé) {#jdpavancé}

Si vous lisez ceci sans avoir fait les parties A, B, C, D, E, F et G (tuto ssh) du jeu de piste, vous avez sans doute raté quelque chose, [repartez de l'étape A1 ci-dessus](#jdpbase).

Une deuxième partie est disponible pour le jeu de piste, mais ces étapes sont plus techniques et moins guidés. Ne vous affolez pas si vous n'arrivez pas à les résoudre.

[étape H1](http://lig-enseignement.imag.fr/jeu-de-piste/lasuite/etape-H1.txt.utf8)
