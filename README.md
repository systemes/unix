# unix-gitlab

Les pages web du cours d'Introduction à Unix est dupliqué sur deux gitlab pages

Depuis l'Ensimag ou avec un VPN <https://systemes.pages.ensimag.fr/unix-gitlab/>

Une copie est accessible sans VPN sur le gitlab de gricad <https://systemes.gricad-pages.univ-grenoble-alpes.fr/unix>


## Tester en local

~~~bash

# 1. Rapatrier le dépot
git clone git@gitlab.ensimag.fr:systemes/unix-gitlab.git
# 2. Installer (Debian/Ubuntu)
apt install mkdocs mkdocs-material mkdocs-material-extensions
# 2. Installer  : https://squidfunk.github.io/mkdocs-material/getting-started/
pip3 install mkdocs-material # installe mkdocs + theme material
# 3. Test en local 
mkdocs serve
firefox http://127.0.0.1:8000 & # modifier vos .md dans .unix-gitlab.git/unix/docs/index.md
~~~
